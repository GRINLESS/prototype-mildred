﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ObjectGame.Management;

namespace ObjectGame {

    /// <summary>
    ///  Large health pickup class.
    /// </summary>
    sealed class HealthPickupLarge : HealthPickupBase {

        //Value of health to apply.
        float healthValue = 3f;

        void Start() {
            //Set the health value.
            base.HealthValue = healthValue;
        }
    }
}
