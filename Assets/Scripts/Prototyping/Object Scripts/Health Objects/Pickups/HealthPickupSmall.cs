﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ObjectGame.Management;

namespace ObjectGame {

    /// <summary>
    ///  Small health pickup class.
    /// </summary>
    sealed class HealthPickupSmall : HealthPickupBase {

        //Value of health to apply.
        float healthValue = 1f;

        void Start() {
            //Set the health value.
            base.HealthValue = healthValue;
        }
    }
}
