﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Player.Systems.Health;

namespace ObjectGame.Management {
    public class HealthPickupBase : MonoBehaviour {

        //Value of health
        [HideInInspector]
        public float HealthValue;
        //Flag for pickup.
        private bool collided = false;

        /// <summary>
        /// Sends the health value to the players health monitor.
        /// </summary>
        private void ApplyHealthToPlayer() {
            if (PlayerSystemHealthController.Instance != null) {
                PlayerSystemHealthController.Instance.ApplyHealth(HealthValue);
            }
            
            Destroy(this.gameObject);
        }

        public void OnCollisionEnter2D(Collision2D collision) {
            //Check if player is collision object.
            if (!collided && collision.gameObject.GetComponent<Controller_Player>()) {
                collided = true;
                ApplyHealthToPlayer();
            }
        }
    }
}
