﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ObjectGame.Management;

namespace ObjectGame {
    /// <summary>
    ///  Mediium health pickup class.
    /// </summary>
    sealed class HealthPickupMedium : HealthPickupBase {

        //Value of health to apply.
        float healthValue = 2f;

        void Start() {
            //Set the health value.
            base.HealthValue = healthValue;
        }
    }
}