﻿using ObjectGame.Interactions;
using DataSaving;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SavePointBehaviour : Interactable, IInteractable {

    public override void InteractionActivate() {
        Debug.Log("Interacted");
        if (DataCarrier.instance != null) {
            Debug.Log("Saving data:");
            DataCarrier.saveDataPlayer.hasSavedGameBefore = true;
            DataCarrier.instance.SaveData();
            Debug.Log("Saving data:Complete");
        }
    }
}
