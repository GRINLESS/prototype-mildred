﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ObjectGame.Data;

public class LifeTimer : MonoBehaviour {

    [Tooltip("The life span of the object")]
    public float maxLifeSpan;
    ILifeSpan iLifeSpan;

    /// <summaryr>
    /// Start the life timer.
    /// </summary>
    /// <param name="lifespan"> The objects life span. </param>
    public void StartTimer(float lifespan) {
        maxLifeSpan = lifespan;
        StartCoroutine(LifespanTimer(maxLifeSpan));
        iLifeSpan = GetComponent<ILifeSpan>();
    }

    /// <summary>
    /// Used to time the life of an object.
    /// </summary>
    /// <param name="lifespan"> the lifespan of the object. </param>
    IEnumerator LifespanTimer(float lifespan) {
        yield return new WaitForSeconds(lifespan);
        iLifeSpan.IsDead();
    }
}
