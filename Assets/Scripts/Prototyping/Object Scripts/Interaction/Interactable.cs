﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ObjectGame.Interactions;
using System;
using ObjectGame.Data;

/// <summary>
/// Component for handling interaction detection.
/// Inherrets from Data_ObjectType for data purposes. 
/// R.C on circle collider so that interaction can be detected.
/// </summary>
[RequireComponent(typeof(CircleCollider2D))]
public class Interactable : Data_ObjectType {

    /// <summary>
    /// The detection circleCollider2D used for this object.
    /// </summary>
    private CircleCollider2D detectionCollider2D;

    private void OnEnable() {
        //Get collider and set to a trigger. 
        detectionCollider2D = GetComponent<CircleCollider2D>();
        detectionCollider2D.isTrigger = true;
    }

    /// <summary>
    /// Override this to add activation logic in derived classes. 
    /// </summary>
    public virtual void InteractionActivate() {
        Debug.Log("Implement override functionality please.");
        throw new NotImplementedException();
    }
}
