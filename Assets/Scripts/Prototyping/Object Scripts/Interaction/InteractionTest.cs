﻿using ObjectGame.Interactions;
using UnityEngine;
using Mildred_UI;

/// <summary>
/// Test class for interaction explanation. 
/// </summary>
public class InteractionTest : Interactable, IInteractable {
    //Use Iinteractable for player to be able to interface with object.
    //Inherrets from Interactable component, which inherretes from Data_Object Type (Object pooling).

    //Colors to swap from during test. 
    public Color color;
    public Color color2;
    //Reference to sprite render. 
    private SpriteRenderer sRenderer;

    public TextAsset asset;

	void Start () {
        //Set render and color.
        sRenderer = GetComponent<SpriteRenderer>();
        sRenderer.color = color;
	}

    public override void InteractionActivate() {
        //Carry through interaction.
        ChangeColor();
        UIManager.Instance.SetIemDescriptionPopup(asset);
    }

    private void ChangeColor() {
        //Swap color. 
        if (sRenderer.color == color) {
            sRenderer.color = color2;
        } else {
            sRenderer.color = color;
        }
    }

}
