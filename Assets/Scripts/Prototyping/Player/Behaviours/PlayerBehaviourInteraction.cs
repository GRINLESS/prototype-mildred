﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using KeyInputMonitoring;
using ObjectGame.Interactions;

public class PlayerBehaviourInteraction : MonoBehaviour {

    /// <summary>
    /// Collider used to detect game objects.
    /// </summary>
    CircleCollider2D interactionTrigger;
    /// <summary>
    /// Last detected interactable object. 
    /// </summary>
    GameObject lastInteractionObject;

	void Start () {
        //Set the collider component up.
        interactionTrigger = GetComponent<CircleCollider2D>();
        interactionTrigger.isTrigger = true;
	}
	
	// Update is called once per frame
	void Update () {
        KeyInputs.Instance.UpdateInteractionInputs();
        //Check for keypress.
        if (KeyInputs.Instance.data_Keycodes.k_Interact.state && !Menu_PauseMenu.Instance.EnableMenu) {
            KeyInputs.Instance.data_Keycodes.k_Interact.state = false;
            if (lastInteractionObject != null) { //Activate.
                lastInteractionObject.GetComponent<IInteractable>().InteractionActivate();
            }
        }
	}

    private void OnTriggerEnter2D(Collider2D collision) {
        //Check for useable collision
        if (collision.gameObject.GetComponent<IInteractable>() != null) {
            lastInteractionObject = collision.gameObject;
        }
    }

    private void OnTriggerExit2D(Collider2D collision) {
        //Check if object should be released. 
        if (collision.gameObject == lastInteractionObject) {
            lastInteractionObject = null;
        }
    }
}
