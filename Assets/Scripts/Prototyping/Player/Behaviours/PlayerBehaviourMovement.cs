﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using KeyInputMonitoring;

namespace Player.Behavior {
    public class PlayerBehaviourMovement : MonoBehaviour {

        /// <summary>
        /// Enum containing directions to be vectorised for movement. 
        /// </summary>
        private enum Directions {
            left = -1,
            right = 1,
            up = 1,
            down = -1
        }

        /// <summary>
        /// The rigidbody component attached to this behaviour.
        /// </summary>
        Rigidbody2D body2D;

        /// <summary>
        /// Movement directions. 
        /// </summary>
        Vector2 direction;

        #region Movement speed fields.

        /// <summary>
        /// The current movement speed assigned. 
        /// </summary>
        float currentMovementSpeed = 0f;
        
        /// <summary>
        /// The maximum movement speed. 
        /// </summary>
        //[HideInInspector]
        public float movementSpeed = 2.5f;

        /// <summary>
        /// The maximum movement speed. 
        /// </summary>
        float startMovementSpeed;

        #endregion

        #region Speed Alteration Fields.

        /// <summary>
        /// Time till max speed.
        /// </summary>
        float timeTillBuildUp = 0.05f;
        /// <summary>
        /// The ammount of frames with which to smooth speed over. 
        /// </summary>
        float speedSmoothing = 60;

        #endregion

        #region Key Hold Fields. 

        /// <summary>
        /// The current key used. 
        /// </summary>
        KeyCode current;

        /// <summary>
        /// the last key used. 
        /// </summary>
        KeyCode last;

        /// <summary>
        /// Flag : Denotes wether input has been held .
        /// </summary>
        bool movementHeld = false;

        /// <summary>
        /// Flag : Denotes wether key release timer is active. 
        /// </summary>
        bool keyReleaseTimerActive = false;

        #endregion

        void Start() {
            body2D = gameObject.GetComponent<Rigidbody2D>();
            if (DataCarrier.instance !=null) {
                movementSpeed = DataCarrier.saveDataPlayer._movementSpeed;
            }
            startMovementSpeed = movementSpeed / speedSmoothing;
            body2D.interpolation = RigidbodyInterpolation2D.Interpolate;
        }

        void Update() {
            if (!Controller_Player.Instance.CanUpdate) {
                return;
            }
            KeyInputs.Instance.UpdateMovementKeys();
            //Calculate movement direction
            direction = CalculateDirection();

            //Check if key is being held. 
            KeyHoldDetection();
        }

        private void LateUpdate() {
            body2D.velocity = direction * currentMovementSpeed;
        }

        /// <summary>
        /// Used to calculate movement direction. 
        /// </summary>
        /// <returns></returns>
        private Vector2 CalculateDirection() {
            last = current;
            bool isSet = false;
            #region Key press monitoring
            if (KeyInputs.Instance.data_Keycodes.k_Down.state && !isSet) {
                if (last != KeyInputs.Instance.data_Keycodes.k_Down.Key) {
                    isSet = true;
                    current = KeyInputs.Instance.data_Keycodes.k_Down.Key;
                    direction.x = 0;
                    direction.y = (float)Directions.down;
                }
                return direction;
            } 
            if (KeyInputs.Instance.data_Keycodes.k_Up.state && !isSet) {
                if (last != KeyInputs.Instance.data_Keycodes.k_Up.Key) {
                    isSet = true;
                    current = KeyInputs.Instance.data_Keycodes.k_Up.Key;
                    direction.x = 0;
                    direction.y = (float)Directions.up;
                }
                return direction;
            }
            if (KeyInputs.Instance.data_Keycodes.k_Left.state && !isSet) {
                if (last != KeyInputs.Instance.data_Keycodes.k_Left.Key) {
                    isSet = true;
                    current = KeyInputs.Instance.data_Keycodes.k_Left.Key;
                    direction.x = (float)Directions.left;
                    direction.y = 0;
                }
                return direction;
            }
            if (KeyInputs.Instance.data_Keycodes.k_Right.state && !isSet) {
                isSet = true;
                if (last != KeyInputs.Instance.data_Keycodes.k_Right.Key) {
                    current = KeyInputs.Instance.data_Keycodes.k_Right.Key;
                    direction.x = (float)Directions.right;
                    direction.y = 0;
                }
                return direction;
            }
            if (!KeyInputs.Instance.data_Keycodes.k_Down.state &&
                !KeyInputs.Instance.data_Keycodes.k_Up.state &&
                !KeyInputs.Instance.data_Keycodes.k_Left.state &&
                !KeyInputs.Instance.data_Keycodes.k_Right.state && !isSet) {
                current = KeyCode.None;
                direction = Vector2.zero;
                return direction;
            }
            #endregion
            return direction;
        }

        #region Key Hold Detection functions
        /// <summary>
        /// Used to detect held keypresses.
        /// </summary>
        private void KeyHoldDetection() {
            //if last and current are the same
            if (current == last) {
                //If they equal Keycode.none start key release.
                if (current == KeyCode.None && last == KeyCode.None && movementHeld) {
                    if (!keyReleaseTimerActive) {
                        //Start timer.
                        keyReleaseTimerActive = true;
                        StartCoroutine(KeyReleased());
                    }
                }
                //If they dont equal keycode.none start movement held. 
                if (current != KeyCode.None && last != KeyCode.None && !movementHeld) {
                    movementHeld = true;
                    StartCoroutine(BuildSpeed());
                    //If the key release was being timed intterupt it.
                    if (keyReleaseTimerActive) {
                        keyReleaseTimerActive = false;
                    }
                }
            }
        }

        /// <summary>
        /// Start key release timer. 
        /// </summary>
        private IEnumerator KeyReleased() {
            yield return new WaitForSeconds(speedSmoothing /** Time.deltaTime*/);
            if (keyReleaseTimerActive) {
                TimerComplete();
            }
        }

        /// <summary>
        /// Release movement held. 
        /// </summary>
        private void TimerComplete() {
            movementHeld = false;
            keyReleaseTimerActive = false;
            StartCoroutine(ReleaseSpeed());
        }

        #endregion

        #region Speed Management

        /// <summary>
        /// Start speed buildup.
        /// </summary>
        /// <returns></returns>
        private IEnumerator BuildSpeed() {
            while (movementSpeed > currentMovementSpeed && movementHeld) {
                currentMovementSpeed += movementSpeed / speedSmoothing;
                yield return new WaitForSeconds(timeTillBuildUp / speedSmoothing * Time.deltaTime);
            }
            currentMovementSpeed = movementSpeed;
        }

        /// <summary>
        /// Start speed release.
        /// </summary>
        /// <returns></returns>
        private IEnumerator ReleaseSpeed() {
            while (currentMovementSpeed > 0 && !movementHeld) {
                currentMovementSpeed -= movementSpeed / speedSmoothing;
                yield return new WaitForSeconds(timeTillBuildUp / speedSmoothing * Time.deltaTime);
            }
            currentMovementSpeed = startMovementSpeed;
        }
        #endregion
    }
}