﻿#define Debug
#undef Debug

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Player.Systems.Health;
using Player.Systems.MentalHealth;
using KeyInputMonitoring;
using Player.Behavior;
using Mildred_UI;

[RequireComponent(typeof(PlayerSystemHealthController))]
[RequireComponent(typeof(PlayerSystemMentalHealthController))]
public class Controller_Player : MonoBehaviour {

    #region Singleton
    /// <summary>
    /// Returns the current instance. 
    /// </summary>
    public static Controller_Player Instance {
        get { return instance; }
        protected set { instance = value; }
    }

    private static Controller_Player instance;

    #endregion

    #region Health
    /// <summary>
    /// Current Health controller.
    /// </summary>
    private PlayerSystemHealthController healthController;
    /// <summary>
    /// Current player health.
    /// </summary>
    public float PlayerHealth = 0;
    /// <summary>
    /// The max player health.
    /// </summary>
    public float PlayerHealth_Max = 7;
    #endregion

    #region Mental Health
    /// <summary>
    /// Current instance of the mental health controller. 
    /// </summary>
    PlayerSystemMentalHealthController mentalHealthController;

    /// <summary>
    /// The current mental health.
    /// </summary>
    public int PlayerMentalHealth {
        get { return playerMentalHealth; }
        set {
            playerMentalHealth = value;
            if (DataCarrier.instance != null) {
                DataCarrier.saveDataPlayer.Data_MentalHealthScale.playerCurrentMH = playerMentalHealth;
            }
        }
    }
    /// <summary>
    /// The current mental health. 
    /// </summary>
    public int playerMentalHealth = 5;
    /// <summary>
    /// The maximum mental health.
    /// </summary>
    public int playerMentalHealth_Max = 5;
    /// <summary>
    /// The minimum mental health.
    /// </summary>
    public int playerMentalHealth_Min = 0;
    #endregion

    #region Flags
    /// <summary>
    /// Set players lifestate.
    /// </summary>
    public bool Alive {
        get { return alive; }
        set { alive = value; }
    }
    /// <summary>
    /// Set the player controllers ability to update. 
    /// </summary>
    public bool CanUpdate {
        get { return canUpdate; }
        set { canUpdate = value; }
    }
    protected bool alive = true;
    protected bool canUpdate = true;

    #endregion

    #region Animation 
    Animator animator;
    #endregion

    void Awake () {
        //Set the singleton.
        Instance = this;

        //Set player data.
        SetPlayerSaveData();

        //Set components.
        healthController = GetComponent<PlayerSystemHealthController>();
        mentalHealthController = GetComponent<PlayerSystemMentalHealthController>();

        //Set up the camera :TODO: Replace once camera is re-written. 
        if (CameraFollow.instance != null) {
            CameraFollow.instance.Target = gameObject;
            CameraFollow.instance.gameObject.transform.parent = this.transform;
            CameraFollow.instance.enabled = false;
        }

        //Set the animator referance
        animator = GetComponent<Animator>();
	}
	
    /// <summary>
    /// Set the save data into the player controller. 
    /// </summary>
    void SetPlayerSaveData() {
        if (DataCarrier.instance != null) {

            //Set player mental health data. 
            playerMentalHealth = DataCarrier.saveDataPlayer.Data_MentalHealthScale.playerCurrentMH;
            playerMentalHealth_Max = DataCarrier.saveDataPlayer.Data_MentalHealthScale.playerHighMH;
            playerMentalHealth_Min = DataCarrier.saveDataPlayer.Data_MentalHealthScale.playerLowMH;

            //Set player health data.
            PlayerHealth = DataCarrier.saveDataPlayer._healthCurrent;
            PlayerHealth_Max = DataCarrier.saveDataPlayer._healthMax;

            //Set the players movement speed. 
            GetComponent<PlayerBehaviourMovement>().movementSpeed = DataCarrier.saveDataPlayer._movementSpeed;
        }
    }

	void Update () {
        #region tests
#if Debug
        //Mental health
        if (Input.GetKeyDown(KeyCode.O)) {
            mentalHealthController.AddMentalHealthValue(1);
        }

        if (Input.GetKeyDown(KeyCode.P)) {
            mentalHealthController.RemoveMentalHealthValue(1);
        }
        //Health tests
        if (Input.GetKeyDown(KeyCode.I)) {
            healthController.ApplyHealth(1);
        }
        if (Input.GetKeyDown(KeyCode.U)) {
            healthController.ApplyDamage(1);
        }
        if (Input.GetKeyDown(KeyCode.Y)) {
            healthController.ApplyFullHealth();
        }
#endif
        #endregion
        //Update health UI
        if (UIManager.Instance != null) {
            UIManager.Instance.SetHealthState(PlayerHealth, PlayerHealth_Max);
        }
        //Check menu state
        CheckIfMenuOpened();
    }

    private void LateUpdate() {
        if (alive) {
            KeyInputs.Instance.FlushToLastState();
        }
        if (!alive) {
            ApplyDeath();
        }
    }

    /// <summary>
    /// Check for menu opening. 
    /// </summary>
    private void CheckIfMenuOpened() {

        KeyInputs.Instance.UpdateReturnInputs();

        if (KeyInputs.Instance.data_Keycodes.k_Return.state) {
            KeyInputs.Instance.data_Keycodes.k_Return.state = false;
            Menu_PauseMenu.Instance.EnableMenu = false;
        }
    }

    /// <summary>
    /// Apply player death.
    /// </summary>
    private void ApplyDeath() {
        if (UIManager.Instance != null) {
            UIManager.Instance.SetTheDeathScreenActive();
        }
    }
}
