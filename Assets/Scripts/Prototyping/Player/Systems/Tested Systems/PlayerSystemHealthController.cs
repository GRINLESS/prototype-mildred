﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Player.Systems.Health {
    /// <summary>
    /// Player health controller class. 
    /// </summary>
    public class PlayerSystemHealthController : BASE_PlayerSystems {

        #region Singleton
        /// <summary>
        /// Returns the current instance.
        /// </summary>
        public static PlayerSystemHealthController Instance {
            get { return instance; }
            private set { instance = value; }
        }
        private static PlayerSystemHealthController instance;
        #endregion

        #region Invincibility frames
        /// <summary>
        /// Are invincibility frames active?
        /// </summary>
        private bool _invincibilityFramesActive = false;
        /// <summary>
        /// The base ammount of invincibility frames per hit.
        /// </summary>
        private float damageInvincibilityFrames = 3f;
        #endregion

        void Awake() {
            //Set the instance for the health monitor.
            if (instance == null) {
                instance = this;
            } else { DestroyImmediate(this); }
        }

        #region Damage Functions
        /// <summary>
        /// Apply damage to the player.
        /// </summary>
        /// <param name="damage"> Damage to apply. </param>
        public void ApplyDamage(int damage) {
            if (!_invincibilityFramesActive) {
                Controller_Player.Instance.PlayerHealth =
                    base.RemoveValue(Controller_Player.Instance.PlayerHealth, damage);
                CallInvincibility(damageInvincibilityFrames);
                CalculateHealth();
            }
        }

        /// <summary>
        /// Apply damage to the player.
        /// </summary>
        /// <param name="damage"> Damage to apply. </param>
        public void ApplyDamage(float damage) {
            if (!_invincibilityFramesActive) {
                Controller_Player.Instance.PlayerHealth =
                    base.RemoveValue(Controller_Player.Instance.PlayerHealth, damage);
                CallInvincibility(damageInvincibilityFrames);
                CalculateHealth();
            }
        }

        /// <summary>
        /// Apply damage to the player & override the stock Iframes.
        /// </summary>
        /// <param name="damage"> Damage to apply. </param>
        /// <param name="OverrideInvincibilityFrames"> Damage to apply. </param>
        public void ApplyDamage(int damage, float OverrideInvincibilityFrames) {
            if (!_invincibilityFramesActive) {
                Controller_Player.Instance.PlayerHealth =
                    base.RemoveValue(Controller_Player.Instance.PlayerHealth, damage);
                CallInvincibility(OverrideInvincibilityFrames);
                CalculateHealth();
            }
        }

        /// <summary>
        /// Apply damage to the player & override the stock Iframes.
        /// </summary>
        /// <param name="damage"> Damage to apply. </param>
        /// <param name="OverrideInvincibilityFrames"> Damage to apply. </param>
        public void ApplyDamage(float damage, float OverrideInvincibilityFrames) {
            if (!_invincibilityFramesActive) {
                Controller_Player.Instance.PlayerHealth =
                    base.RemoveValue(Controller_Player.Instance.PlayerHealth, damage);
                CallInvincibility(OverrideInvincibilityFrames);
                CalculateHealth();
            }
        }
        #endregion

        #region Health Functions
        /// <summary>
        /// Add health to the player.
        /// </summary>
        /// <param name="value"> Value to add. </param>
        public void ApplyHealth(int value) {
            Controller_Player.Instance.PlayerHealth = base.AddValue(Controller_Player.Instance.PlayerHealth, value);
            CalculateHealth();
        }

        /// <summary>
        /// Add health to the player.
        /// </summary>
        /// <param name="value"> Value to add. </param>
        public void ApplyHealth(float value) {
            Controller_Player.Instance.PlayerHealth = base.AddValue(Controller_Player.Instance.PlayerHealth, value);
            CalculateHealth();
        }

        /// <summary>
        /// Apply full health to player. 
        /// </summary>
        public void ApplyFullHealth() {
            Controller_Player.Instance.PlayerHealth = Controller_Player.Instance.PlayerHealth_Max;
            CalculateHealth();
        }
        #endregion

        #region Invincibility Functions

        /// <summary>
        /// Set an ammount of invincibility to the player character.
        /// </summary>
        /// <param name="IframesAmmount"> The length of the iframes. </param>
        public void CallInvincibility(float IframesAmmount) {
            Debug.Log("");
            _invincibilityFramesActive = true;
            StartCoroutine(InvincibilityFrames(IframesAmmount));
        }

        /// <summary>
        /// Start Invincibility frames.
        /// </summary>
        /// <param name="invincibilityFrames"> The time to set the player invincible for. </param>
        private IEnumerator InvincibilityFrames(float invincibilityFrames) {
            float timer = 0;
            while (invincibilityFrames > timer) {
                timer += Time.deltaTime;
                yield return null;
            }
            _invincibilityFramesActive = false;
            yield return true;
        }

        #endregion

        #region Health calculations

        /// <summary>
        /// Calculates the health value for the player.
        /// </summary>
        private void CalculateHealth() {
            Controller_Player.Instance.PlayerHealth =
                base.CalculateValue(Controller_Player.Instance.PlayerHealth_Max,
                0f,
                Controller_Player.Instance.PlayerHealth);

            if (Controller_Player.Instance.PlayerHealth == 0) {
                DeathOccured();
            }
        }

        #endregion

        /// <summary>
        /// Set the players life state to dead.
        /// </summary>
        private void DeathOccured() {
            Controller_Player.Instance.Alive = false;
        }    
    }
}
