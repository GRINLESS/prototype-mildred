﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Player.MentalHealth;

namespace Player.Systems.MentalHealth {
    /// <summary>
    /// Mental health controller class.
    /// </summary>
    public class PlayerSystemMentalHealthController : BASE_PlayerSystems {

        /// <summary>
        /// Add mental health.
        /// </summary>
        /// <param name="value"></param>
        public void AddMentalHealthValue(int value) {
            Controller_Player.Instance.PlayerMentalHealth = 
                AddValue(Controller_Player.Instance.PlayerMentalHealth, value);
            CalculateMentalHealth();
        }

        /// <summary>
        /// Remove mental health.
        /// </summary>
        /// <param name="value"></param>
        public void RemoveMentalHealthValue(int value) {
            Controller_Player.Instance.PlayerMentalHealth = 
                RemoveValue(Controller_Player.Instance.PlayerMentalHealth, value);
            CalculateMentalHealth();
        }

        /// <summary>
        /// Calculate mental health.
        /// </summary>
        public void CalculateMentalHealth() {
            Controller_Player.Instance.PlayerMentalHealth =
                CalculateValue(Controller_Player.Instance.playerMentalHealth_Max,
                Controller_Player.Instance.playerMentalHealth_Min, Controller_Player.Instance.PlayerMentalHealth);
        }
    }
}
