﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Player.Systems {
    public class BASE_PlayerSystems : MonoBehaviour {

        #region Calculation Helper methods
        /// <summary>
        /// Adds the values together.
        /// </summary>
        /// <returns> The total value of value1 + value2.</returns>
        protected float AddValue(float value1, int value2) {
            return value1 + value2;
        }
        /// <summary>
        /// Adds the values together.
        /// </summary>
        /// <returns> the total value of value1 + value2.</returns>
        protected float AddValue(float value1, float value2) {
            return value1 + value2;
        }
        /// <summary>
        /// Adds the values together.
        /// </summary>
        /// <returns> the total value of value1 + value2.</returns>
        protected int AddValue(int value1, int value2) {
            return value1 + value2;
        }
        /// <summary>
        /// Subtracts value2 from value1.
        /// </summary>
        /// <param name="value1"> value to subtract from.</param>
        /// <param name="value2"> total value to subtract.</param>
        /// <returns> The result of subtraction. </returns>
        protected float RemoveValue(float value1, int value2) {
            return value1 - value2;
        }
        /// <summary>
        /// Subtracts value2 from value1.
        /// </summary>
        /// <param name="value1"> Value to subract from. </param>
        /// <param name="value2"> Value to be subtracted. </param>
        /// <returns> Value of subtraction. </returns>
        protected float RemoveValue(float value1, float value2) {
            return value1 - value2;
        }
        /// <summary>
        /// Subtracts value2 from value1.
        /// </summary>
        /// <param name="value1"> Value to subract from. </param>
        /// <param name="value2"> Value to be subtracted. </param>
        /// <returns> Value of subtraction. </returns>
        protected int RemoveValue(int value1, int value2) {
            return value1 - value2;
        }
        /// <summary>
        /// Use to check a value against two constraining values.
        /// </summary>
        /// <param name="maxValue"> The maximum value. </param>
        /// <param name="minValue"> The minimum value. </param>
        /// <param name="currentValue"> The value to be checked. </param>
        /// <returns> a value calculated against its constraints. </returns>
        protected float CalculateValue(int maxValue, int minValue, float currentValue) {
            if (currentValue > maxValue) {
                currentValue = maxValue;
            }
            if (currentValue <= minValue) {
                currentValue = minValue;
            }
            return currentValue;
        }
        /// <summary>
        /// Use to check a value against two constraining values.
        /// </summary>
        /// <param name="maxValue"> The maximum value. </param>
        /// <param name="minValue"> The minimum value. </param>
        /// <param name="currentValue"> The value to be checked. </param>
        /// <returns> a value calculated against its constraints. </returns>
        protected float CalculateValue(float maxValue, float minValue, float currentValue) {
            if (currentValue > maxValue) {
                currentValue = maxValue;
            }
            if (currentValue <= minValue) {
                currentValue = minValue;
            }
            return currentValue;
        }
        /// <summary>
        /// Use to check a value against two constraining values.
        /// </summary>
        /// <param name="maxValue"> The maximum value. </param>
        /// <param name="minValue"> The minimum value. </param>
        /// <param name="currentValue"> The value to be checked. </param>
        /// <returns> a value calculated against its constraints. </returns>
        protected int CalculateValue(int maxValue, int minValue, int currentValue) {
            if (currentValue > maxValue) {
                currentValue = maxValue;
            }
            if (currentValue <= minValue) {
                currentValue = minValue;
            }
            return currentValue;
        }
        #endregion
    }
}
