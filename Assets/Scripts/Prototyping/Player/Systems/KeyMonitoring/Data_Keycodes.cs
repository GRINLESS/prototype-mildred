﻿using Input_KeyFunctionality;
using UnityEngine;

namespace KeyInputMonitoring {
    /// <summary>
    /// Class for holding assigned key data. 
    /// </summary>
    [System.Serializable]
    public class Data_Keycodes {
        public KeyAssignData
                    k_Up = new KeyAssignData(KeyCode.W),
                    k_Down = new KeyAssignData(KeyCode.S),
                    k_Left = new KeyAssignData(KeyCode.A),
                    k_Right = new KeyAssignData(KeyCode.D),
                    k_Interact = new KeyAssignData(KeyCode.Space),
                    k_Return = new KeyAssignData(KeyCode.Escape);

    }
}
