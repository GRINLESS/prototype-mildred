﻿using UnityEngine;
using UnityEngine.UI;
using Menu.KeyRebind;

namespace Input_KeyFunctionality {
    //Data class for storing and using data inputs
    [System.Serializable]
    public class KeyAssignData {
        public KeyCode Key; // Used to hold key assigned.
        public bool state; //bool for the current keypress state.
        public bool lastState; // bool for the last key state.

        //Constructor, assigns passed keycode in, sets starting keystates.
        public KeyAssignData(KeyCode key) {
            Key = key;
            lastState = state = false;
        }

        /// <GetNewKey>
        /// Used to start allocation & detection for key rebinding. 
        /// </GetNewKey>
        /// <param name="text"> A text output field for the key. </param>
        /// <returns> The newly assigned key. </returns>
        public KeyCode GetNewKey(Text text, MenuAnimation_DisplayWaitTimer timer) {
            Get_KeyOP GKO = new GameObject().AddComponent<Get_KeyOP>();
            GKO.name = "KEYREBIND_OP : " + Key.ToString();
            GKO.KeyRebind(this, text, timer);
            return Key;
        }
    }
}
