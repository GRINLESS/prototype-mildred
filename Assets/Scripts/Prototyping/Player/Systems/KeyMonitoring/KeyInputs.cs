﻿using UnityEngine;

namespace KeyInputMonitoring {
    //keycode input list.
    public class KeyInputs : MonoBehaviour {

        //Using a static here so that the key inputs can be accessed easily for events.
        private static KeyInputs instance;
        //public static KeyInputs Instance {
        //    get { return instance; }
        //    private set { instance = value; }
        //}
        public static KeyInputs Instance {
            get {
                if (instance == null) {
                    instance = KeyInputsCreator();
                }
                return instance;
            }
        }

        //Currently assigned keys.
        [Tooltip("Current key assignments.")]
        public Data_Keycodes data_Keycodes;

        public void Awake() {
            //Set singleton.
            if (instance == null) {
                instance = this;
            } else {
                DestroyImmediate(this);
            }
            if (DataCarrier.instance != null) {
                data_Keycodes = DataCarrier.saveDataGameConfig.data_Keycodes;
            } else {
                data_Keycodes = new Data_Keycodes();
            }
        }

        #region Player Input Updates

        /// <summary>
        /// Update player movement keys. 
        /// </summary>
        public void UpdateMovementKeys() {
            
            //Check for key press events (Norm & alt, Up & Down key events).
            #region Left Input
            if (Input.GetKeyDown(data_Keycodes.k_Left.Key) || Input.GetKey(data_Keycodes.k_Left.Key)) {
                data_Keycodes.k_Left.state = true;
            } else{
                data_Keycodes.k_Left.state = false;
            }
            #endregion

            #region Right Input
            if (Input.GetKeyDown(data_Keycodes.k_Right.Key) || Input.GetKey(data_Keycodes.k_Right.Key)) {
                data_Keycodes.k_Right.state = true;
            } else if (Input.GetKeyUp(data_Keycodes.k_Right.Key)) {
                data_Keycodes.k_Right.state = false;
            }
            #endregion

            #region Up Input
            if (Input.GetKeyDown(data_Keycodes.k_Up.Key) || Input.GetKey(data_Keycodes.k_Up.Key)) {
                data_Keycodes.k_Up.state = true;
            } else if (Input.GetKeyUp(data_Keycodes.k_Up.Key)) {
                data_Keycodes.k_Up.state = false;
            }
            #endregion

            #region Down Input

            if (Input.GetKeyDown(data_Keycodes.k_Down.Key) || Input.GetKey(data_Keycodes.k_Down.Key)) {
                data_Keycodes.k_Down.state = true;
            } else if (Input.GetKeyUp(data_Keycodes.k_Down.Key)) {
                data_Keycodes.k_Down.state = false;
            }

            #endregion
        }

        /// <summary>
        /// Update the player interaction key. 
        /// </summary>
        public void UpdateInteractionInputs() {
            #region Check Keys

            if (Input.GetKeyDown(data_Keycodes.k_Interact.Key)
                || Input.GetKeyDown(data_Keycodes.k_Interact.Key) && data_Keycodes.k_Interact.lastState) {
                data_Keycodes.k_Interact.state = true;
            }

            if (Input.GetKeyUp(data_Keycodes.k_Interact.Key)) {
                data_Keycodes.k_Interact.state = false;
            }

            #endregion
        }

        /// <summary>
        /// Update the return input.
        /// </summary>
        public void UpdateReturnInputs() {
            #region Return Input
            if (Input.GetKeyDown(data_Keycodes.k_Return.Key)
                || Input.GetKeyDown(data_Keycodes.k_Return.Key) && data_Keycodes.k_Return.lastState) {
                data_Keycodes.k_Return.state = true;
            }

            if (Input.GetKeyUp(data_Keycodes.k_Return.Key)) {
                data_Keycodes.k_Return.state = false;
            }
            #endregion
        }

        #endregion

        /// <summary>
        /// Updates the menu inputs.
        /// </summary>
        public void UpdateInputsMenu() {
            #region Down Input
            if (Input.GetKeyUp(data_Keycodes.k_Down.Key)) {
                data_Keycodes.k_Down.state = true;
            }

            #endregion

            #region Up Input
            if (Input.GetKeyUp(data_Keycodes.k_Up.Key)) {
                data_Keycodes.k_Up.state = true;
            }

            #endregion

            #region Right Input
            if (Input.GetKeyUp(data_Keycodes.k_Right.Key)) {
                data_Keycodes.k_Right.state = true;
            }

            #endregion

            #region Left Input
            if (Input.GetKeyUp(data_Keycodes.k_Left.Key)) {
                data_Keycodes.k_Left.state = true;
            }

            #endregion

            #region Interact Input
            if (Input.GetKeyUp(data_Keycodes.k_Interact.Key)) {
                data_Keycodes.k_Interact.state = true;
            }
            #endregion

            #region Pause Input
            if (Input.GetKeyUp(data_Keycodes.k_Return.Key)) {
                data_Keycodes.k_Return.state = true;
            }
            #endregion
        }

        /// <summary>
        /// Flushes current input info to last input references. 
        /// </summary>
        public void FlushToLastState() {
            data_Keycodes.k_Interact.lastState = data_Keycodes.k_Interact.state;
            data_Keycodes.k_Left.lastState = data_Keycodes.k_Left.state;
            data_Keycodes.k_Right.lastState = data_Keycodes.k_Right.state;
            data_Keycodes.k_Return.lastState = data_Keycodes.k_Return.state;
            data_Keycodes.k_Interact.lastState = data_Keycodes.k_Interact.state;
        }
        public static KeyInputs KeyInputsCreator() {
            GameObject keyInputs = new GameObject();
            keyInputs.name = "Key Inputs";
            keyInputs.AddComponent<KeyInputs>();
            DontDestroyOnLoad(keyInputs);
            return keyInputs.GetComponent<KeyInputs>();         
        }
    }
}
