﻿#define Debug
#undef Debug

using DataSaving;
using UnityEngine;

namespace ResolutionManagement {
    public class ResolutionManager : MonoBehaviour {

        public static ResolutionManager instance;
        public SaveDataGameConfig SDGC;
        private SetResolutions referance = new SetResolutions();

        void Awake() {
            instance = this;
            SDGC = DataCarrier.saveDataGameConfig;
        }

        void Update() {
            if (Input.GetKeyUp(KeyCode.F1)) {
                SetFullscreen();
            }
        }

        //Used to set fullscreen
        public void SetFullscreen() {

            SDGC.fullscreen = !SDGC.fullscreen;
            Screen.SetResolution(SDGC.resolution._width,
                SDGC.resolution._height,
                SDGC.fullscreen);
        }
    }
}
