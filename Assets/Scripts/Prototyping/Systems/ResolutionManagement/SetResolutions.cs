﻿using Resolution;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ResolutionManagement {
    [System.Serializable]
    public class SetResolutions {
        //Resolutions catered to.
        public List<_Resolution> resolutions = new List<_Resolution>() {
            new _Resolution("1366x768", 1366, 768),
            new _Resolution("1600x900", 1600, 900),
            new _Resolution("1920x1080", 1920, 1080)};
    }
}
