﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ObjectGame.Management;

public class Test_Spawner : MonoBehaviour {

    public GameObject spawnType; //The object to spawn.
    public GameObject spawnPoint; //The spawnpoint where object should be placed. 

    public float delayBetweenSpawn; //The delay between spawns. 

    //Starts initial coroutine call. 
	void Start () {
        StartCoroutine(SpawnTimer());
	}

    /// <SpawnTimer>
    /// >Wait for spawn delay.
    /// >Call spawn method.
    /// </summary>
    /// <returns> Time delay. </returns>
    IEnumerator SpawnTimer() {
        yield return new WaitForSeconds(delayBetweenSpawn);
        Spawn();        
    }

    /// <Spawn>
    /// >Gets object from the object pool. 
    /// >Sets the object active. 
    /// >Starts the spawn timer again. 
    /// </summary>
    private void Spawn() {
        GameObject spawn = ObjectPoolManager.Instance._GetObject(spawnType, spawnPoint.transform);
        StartCoroutine(SpawnTimer());
    }
}
