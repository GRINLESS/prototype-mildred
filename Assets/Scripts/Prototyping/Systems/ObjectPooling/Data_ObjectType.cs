﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ObjectGame.Data {
    [System.Serializable]
    public class Data_ObjectType : MonoBehaviour {
        public int OBJ_ID;
        public bool active;
    }
}
