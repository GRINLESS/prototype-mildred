﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ObjectGame.Data;
using ObjectGame.Management;

namespace ObjectGame.Management {

    /// <summary>
    /// Main class for object pooling management functionality. 
    /// </summary>
    public class ObjectPoolManager : MonoBehaviour {
        #region Singleton
        private static ObjectPoolManager instance;
        public static ObjectPoolManager Instance {
            get { return instance; }
            internal set { instance = value; }
        }
        #endregion

        /// <summary>
        /// List of object data types in object pool.
        /// </summary>
        public List<ObjectTypeList> objectListPool = new List<ObjectTypeList>();

        // Awake is used to set the singleton.
        private void Awake() {
            if (Instance == null) {
                instance = this;
            } else {
                DestroyImmediate(this);
            }
        }
        
        /// <summary>
        /// Used to retrive or create a matched gameobject to return to caller method. 
        /// </summary>
        /// <param name="_object"> Gameobject type to find or create. </param>
        /// <param name="transform"> Where to create the gameobject. </param>
        /// <returns></returns>
        public GameObject _GetObject(GameObject _object, Transform transform) {
            GameObject GameObject_Return = null; //Create gameobject reference to assign to.
            ObjectTypeList O_TL = null; //Create ObjectTypeList Reference
            Data_ObjectType D_OT = _object.GetComponent<Data_ObjectType>(); //Set the D_OT
            O_TL = Find_OTL(D_OT, _object.name); //Set the O_TL by calling O_TL search function.

            foreach (GameObject obj in O_TL.list) { //Search through O_TL to see if a object can be returned.
                if (!obj.activeSelf) { //If not active then it can be used.
                    GameObject_Return = obj; //Set the object.
                    break;
                }
            }
            if (GameObject_Return == null) { //If no useable object was found
                GameObject_Return = CreateItem(_object, transform, O_TL); //Object creation method is called and new obj set.
            }
            
            SetItemTransform(transform, GameObject_Return);
            GameObject_Return.GetComponent<Data_ObjectType>().active = true;
            GameObject_Return.SetActive(true);
            return GameObject_Return; //Then we return the object.
        }

        /// <summary>
        /// Retuns an active object to the object pool.
        /// </summary>
        /// <param name="obj"> Object being returned to the pool. </param>
        public void _ReturnObject(GameObject obj) {
            Data_ObjectType D_OT = gameObject.GetComponent<Data_ObjectType>();
            obj.transform.parent = this.transform;
            obj.GetComponent<Data_ObjectType>().active = false;
            obj.SetActive(false);
        }

        #region List Methods

        /// <summary>
        /// Used to find the Object type list for current object. 
        /// </summary>
        /// <param name="D_OT"> The current object data class. </param>
        /// <param name="name"> The current object name (Used for Idenifying object list type in editor.). </param>
        /// <returns></returns>
        private ObjectTypeList Find_OTL(Data_ObjectType D_OT, string name) {
            ObjectTypeList listType = null; //Set the list reference to null for easy comparison later.

            foreach (ObjectTypeList OTL in objectListPool) { //Search through the object list pool
                if (OTL.ID_Obj == D_OT.OBJ_ID) {
                    listType = OTL;
                    break; //If found, we set it and then break loop.
                }
            }
            if (listType == null) { //If the list wasn't found.
                listType = Add_OTL(D_OT.OBJ_ID, name); //Create list and set it.
            }
            return listType; //Return the list.
        }

        private ObjectTypeList Add_OTL(int OBJ_ID,string nameOfObjType) {
            ObjectTypeList OTL_NEW; //Set a reference.
            OTL_NEW = new ObjectTypeList(OBJ_ID, nameOfObjType); //Create the OTL.
            objectListPool.Add(OTL_NEW); //Add to the ObjectListPool. 
            return OTL_NEW; //Return the OTL.
        }
        #endregion

        #region Object Management

        /// <summary>
        /// Used to instaniate the object passed in.
        /// </summary>
        /// <param name="_type"> Game object to instantiate. </param>
        /// <param name="transform"> Transform used to set the gameobkect at. </param>
        /// <param name="OTL"> ObjectTypeList used to assign the object to. </param>
        /// <returns></returns>
        private GameObject CreateItem(GameObject _type, Transform transform, ObjectTypeList OTL) {
            GameObject gameobject_Return; //Set the reference.

            gameobject_Return = Instantiate(_type); //Create the object.
            SetItemTransform(transform, gameobject_Return);
            OTL.AddObject(gameobject_Return); //Add the object to its list.
            return gameobject_Return; //Return the object.
        }

        /// <summary>
        /// Used to set the gameobjects transform. 
        /// </summary>
        /// <param name="transform"> Where the object transform is to be set. </param>
        /// <param name="_obj"> The object that needs to be set. </param>
        private void SetItemTransform(Transform transform, GameObject _obj) {
            _obj.transform.parent = this.transform;
            _obj.GetComponent<Transform>().transform.position = transform.position;
        }
        #endregion
    }
}

/// <summary>
/// Class for object pooling calls. 
/// </summary>
public class ObjectPoolCalls : ObjectPoolManager {

    /// <summary>
    /// Gets and returns a game object from gameobject.
    /// </summary>
    /// <param name="Object"></param>
    /// <param name="transform"></param>
    /// <returns></returns>
    public static GameObject GetObject(GameObject Object, Transform transform) {
        return ObjectPoolManager.Instance._GetObject(Object, transform);
    }

    /// <summary>
    /// Used to return object from a pool. 
    /// </summary>
    /// <param name="Object"> Object to return to the object pool. </param>
    public static void ReturnObject(GameObject Object) {
        ObjectPoolManager.Instance._ReturnObject(Object);
    }
}
