﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ObjectGame.Management {
    [System.Serializable]
    public class ObjectTypeList {
        public string _name;
        public int ID_Obj; //Object type ID
        public List<GameObject> list = new List<GameObject>(); //List of objects with the ID

        public ObjectTypeList(int _ID_OBJ, string name) { //Constructer for the list.
            ID_Obj = _ID_OBJ;
            _name = name;
        }

        public void AddObject(GameObject obj) { //Used for adding object to the list. 
            list.Add(obj);
        }
        public void ClearList() { //Used for clearing the list. 
            list.Clear();
        }
    }
}
