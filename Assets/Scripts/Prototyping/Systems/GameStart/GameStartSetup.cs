﻿using DataSaving;
using UnityEngine;
using UnityEngine.SceneManagement;
using ResolutionManagement;

public class GameStartSetup : MonoBehaviour {

    DataCarrier dataCarrier;
    public int buildIndexToLoadOnCompletion;

    public void Awake() {
        CreateDataCarrier();
    }

    private void Update() {
        //If the save data firstOpen flag is true
        if (DataCarrier.saveDataGameConfig.firstExecution) {
            //Do setup method calls. 
            FirstOpenResolutionSetup();
        } else {
            Screen.SetResolution(
                DataCarrier.saveDataGameConfig.resolution._width,
                DataCarrier.saveDataGameConfig.resolution._height,
                DataCarrier.saveDataGameConfig.fullscreen);
        }
        DataCarrier.instance.SaveData();

        OnCompletion();
    }

    /// <summary>
    /// Creates the data carrying object.
    /// Adds the DataCarrier script. 
    /// Calls DontDestroyOnLoad so the object is able to survive scene loading. 
    /// </summary>
    void CreateDataCarrier() {
        GameObject obj = new GameObject();
        obj.name = "DataCarrier";
        dataCarrier = obj.AddComponent<DataCarrier>();
        DontDestroyOnLoad(obj);
    }

    /// <summary>
    /// Sets the most optimal resolution for user. 
    /// </summary>
    private void FirstOpenResolutionSetup() {
        DetectResolution();
    }

    /// <summary>
    /// Used to detect optimal resolution for game. 
    /// </summary>
    private void DetectResolution() {
        ResolutionManagement.SetResolutions resList = 
            new ResolutionManagement.SetResolutions();
        foreach (Resolution._Resolution resolution in resList.resolutions) {
            if (resolution._width >= Screen.currentResolution.width) {
                DataCarrier.saveDataGameConfig.resolution = resolution;
                break;
            }
        }
    }

    private void OnCompletion() {
        //TODO: replace with call to level manager. 
        SceneManager.LoadScene(buildIndexToLoadOnCompletion);
    }
}