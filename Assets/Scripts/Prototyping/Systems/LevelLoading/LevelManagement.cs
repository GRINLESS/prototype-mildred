﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManagement : MonoBehaviour {

    public static readonly int intro = 0, main = 1, level1 = 2;

    public static void LoadLevel(int id) {
        if (Controller_Player.Instance != null) {
            Controller_Player.Instance.CanUpdate = false;
        }
        if (DataCarrier.instance != null) {
            DataCarrier.instance.SaveData();
        }
        UnityEngine.SceneManagement.SceneManager.LoadScene(id);
    }
}
