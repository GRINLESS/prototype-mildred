﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Player.Systems.Health;
using Player.Behavior;

//Used to test damage functions.
public class ApplyDamageTest : MonoBehaviour {
    public void OnCollisionEnter2D(Collision2D collision) {
        if (collision.gameObject.GetComponent<Controller_Player>()) {
            PlayerSystemHealthController.Instance.ApplyDamage(1);
        }
    }
}
