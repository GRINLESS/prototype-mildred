﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Real simple camera follow script.

    /// <summary>
    /// Camera Follow component script. 
    /// </summary>
public class CameraFollow : MonoBehaviour {

    [Tooltip("the camera instance.")]
    public static CameraFollow instance;
    [Tooltip("The target to folllow.")]
    public GameObject Target;
    [Tooltip("Camerera movement speed.")]
    public float speed = 2.0f;

    public void Awake() {
        instance = this;
    }

    void Update () {
        if (Target != null) {
            float interp = speed * Time.deltaTime;

            Vector3 position = this.transform.position;

            position.y = Mathf.Lerp(this.transform.position.y, Target.transform.position.y, interp);
            position.x = Mathf.Lerp(this.transform.position.x, Target.transform.position.x, interp);

            this.transform.position = position;
        }
	}
}
