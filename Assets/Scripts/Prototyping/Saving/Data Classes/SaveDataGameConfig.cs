﻿using KeyInputMonitoring;
using Resolution;
using ResolutionManagement;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DataSaving {
    /// <SaveDataConfig>
    /// Class containing player setting data. 
    /// </summary>
    [System.Serializable]
    public class SaveDataGameConfig {
        public bool firstExecution = true;
        public Data_Keycodes data_Keycodes = new Data_Keycodes();
        //public ResolutionManager._Resolution resolution = new ResolutionManager._Resolution("", 0, 0);
        public _Resolution resolution = new _Resolution("", 0, 0);
        public bool fullscreen = true;
    }
}
