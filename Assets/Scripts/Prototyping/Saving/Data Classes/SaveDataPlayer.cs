﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Player.MentalHealth;

namespace DataSaving {
    [System.Serializable]
    public class SaveDataPlayer {

        public bool hasSavedGameBefore = false;

        //Health value data.
        public float _healthCurrent = 3;
        public float _healthMax = 3;

        public float _movementSpeed = 2.5f;

        public Data_MentalHealthScale Data_MentalHealthScale = new Data_MentalHealthScale(); 

        //empty constructor for new save file
        public SaveDataPlayer() {

        }

        //Overloaded health data constructor. (For testing purposes)
        public SaveDataPlayer(float healthCurrent, float healthMax) {
            _healthCurrent = healthCurrent;
            _healthMax = healthMax;
        }
    }
}
