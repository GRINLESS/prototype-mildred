﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Player.MentalHealth {
    /// <summary>
    /// Data class, carries mental health event values.
    /// </summary>
    [System.Serializable]
    public class Data_MentalHealthEvent {
        public string eventTitle;
        public bool hasBeenActivated;
        public int valueOfEvent;

        public Data_MentalHealthEvent(string _eventTitle, bool activated, int value) {
            eventTitle = _eventTitle;
            valueOfEvent = value;
            hasBeenActivated = activated;
        }
    }
}
