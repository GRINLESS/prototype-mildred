﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Player.MentalHealth{
    /// <summary>
    /// Data class for mental health values. 
    /// </summary>
    [System.Serializable]
    public class Data_MentalHealthScale {
        //Low & high values for player mental health.
        public int playerLowMH = 3;
        public int playerHighMH = 3;
        //Players current mental health value. 
        public int playerCurrentMH = 5;
        //Flags for endings. 
        public bool badMH = false;
        public bool goodMH = true;
        //List of events. Using the data class designed for mental health.
        Data_MentalHealthEvent[] list = 
            new Data_MentalHealthEvent[] {
                new Data_MentalHealthEvent( "TestEvent" , false, -1)
            };
    }
}