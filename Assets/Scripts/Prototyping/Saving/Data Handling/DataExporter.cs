﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace DataSaving.LowLevel {
    /// <summary>
    /// Class containing low level data parsing. 
    /// </summary>
    public static class DataExporter {

        /// <summary>
        /// The string used to create the players save file. 
        /// </summary>
        public static string fileName_PlayerData = "/KILLME.Grinless";

        #region Data Functions
        /// <summary>
        /// Takes a player data class and serializes it.
        /// </summary>
        /// <param name="SDE"> The players saveable data. </param>
        public static void SaveData(SaveDataExport SDE) {
            
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Create(Application.persistentDataPath + fileName_PlayerData);
            bf.Serialize(file, SDE);
            file.Close();
        }

        /// <summary>
        /// Binary deserialization used to pull 
        /// & return player data class. 
        /// </summary>
        /// <returns> Player data class </returns>
        public static SaveDataExport LoadData() {
            SaveDataExport SDE;

            if (File.Exists(Application.persistentDataPath + fileName_PlayerData)) {
                BinaryFormatter bf = new BinaryFormatter();
                FileStream file = File.Open(Application.persistentDataPath + fileName_PlayerData, FileMode.Open);
                SDE = (SaveDataExport)bf.Deserialize(file);
                file.Close();
            } else {
                SDE = new SaveDataExport();
            }
            return SDE;
        }
        #endregion
    }
}
