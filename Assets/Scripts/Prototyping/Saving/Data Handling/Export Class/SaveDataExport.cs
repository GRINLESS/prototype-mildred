﻿using DataSaving;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class used for holding data to be parsed.
/// </summary>
[System.Serializable]
public class SaveDataExport {
    /// <summary>
    /// Marks wether the game has been run before.
    /// </summary>
    public bool firstOpen = true;
    /// <summary>
    /// The players configuration data.
    /// </summary>
    public SaveDataGameConfig saveDataConfig = new SaveDataGameConfig();
    /// <summary>
    /// The players save game.
    /// </summary>
    public SaveDataPlayer saveDataPlayer = new SaveDataPlayer();
}
