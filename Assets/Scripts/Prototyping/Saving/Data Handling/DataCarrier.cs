﻿#define Debug
#undef Debug

using DataSaving.LowLevel;
using UnityEngine;

/// <DataCarrier>
/// Used to carry game data from scene to scene.
/// </DataCarrier>
public class DataCarrier : Base_DataCarrier {

    public static DataCarrier instance = null;

    private void Awake() {
        //Set instance.
        if (instance == null) {
            instance = this;
        } else {
            DestroyImmediate(this);
        }
        
        //Load game data.
        LoadDataToCarrier();
    }

    #region Public Data Calls

    /// <summary>
    /// Save the data.
    /// </summary>
    public void SaveData() {
        //Set the first execution flag.
        if (saveDataGameConfig.firstExecution) {
            saveDataGameConfig.firstExecution = false;
        }
        //Export data. 
        DataExporter.SaveData(ExportData());
    }

    /// <summary>
    /// Load data. 
    /// </summary>
    public void LoadData() {
        LoadDataToCarrier();
    }

    #endregion
}

/// <summary>
/// Class containing low level data handling.
/// </summary>
namespace DataSaving.LowLevel {
    public class Base_DataCarrier : MonoBehaviour {
        /// <summary>
        /// Data class for exporting data.
        /// </summary>
        internal SaveDataExport export = new SaveDataExport();

        #region Data Clases
        /// <summary>
        /// Reference to the SaveDataGameConfig
        /// </summary>
        public static SaveDataGameConfig saveDataGameConfig {
            get { return DataCarrier.instance._saveDataGameConfig; }
        }
        public SaveDataGameConfig _saveDataGameConfig;
        /// <summary>
        /// Reference to the SaveDataPlayer
        /// </summary> 
        public static SaveDataPlayer saveDataPlayer {
            get { return DataCarrier.instance._saveDataPlayer; }
        }
        public SaveDataPlayer _saveDataPlayer;
        #endregion

        #region Data Loading
        public void SetNewPlayerSaveData() {
            _saveDataPlayer = new SaveDataPlayer();
        }
        /// <LoadDataToCarrier>
        /// Pull data from save file. 
        /// </summary>
        internal void LoadDataToCarrier() {
            //Load data 
            SaveDataExport SDE = DataExporter.LoadData();
            //load the 
            _saveDataGameConfig = SDE.saveDataConfig;
            _saveDataPlayer = SDE.saveDataPlayer;
        }

        #endregion

        #region Data Saving

        /// <summary>
        /// Complies all the current save data to a SaveDataExport class.
        /// </summary>
        /// <returns> the compiled SaveDataExport. </returns>
        internal SaveDataExport ExportData() {
            //Pull the save data.
            PullConfigData();
            PullGameData();
            //Set the export data.
            SaveDataExport _export = export;
            export.saveDataConfig = saveDataGameConfig;
            export.saveDataPlayer = saveDataPlayer;
            //Return the Save data export to caller. 
            return _export;
        }
        
        #endregion

        #region Data Pulling Functionality

        /// <summary>
        /// Pull config data from game.
        /// </summary>
        private void PullConfigData() {
            if (KeyInputMonitoring.KeyInputs.Instance != null) {
                DataCarrier.saveDataGameConfig.data_Keycodes = KeyInputMonitoring.KeyInputs.Instance.data_Keycodes;
            }
        }

        /// <summary>
        /// Pull game data. 
        /// </summary>
        private void PullGameData() {
            //Pull the player data
            if (DataCarrier.instance != null && Controller_Player.Instance != null) {
                saveDataPlayer._healthCurrent = Controller_Player.Instance.PlayerHealth;
                saveDataPlayer._healthMax = Controller_Player.Instance.PlayerHealth_Max;
                saveDataPlayer.Data_MentalHealthScale.playerCurrentMH = Controller_Player.Instance.playerMentalHealth;
                saveDataPlayer.Data_MentalHealthScale.playerHighMH = Controller_Player.Instance.playerMentalHealth_Max;
                saveDataPlayer.Data_MentalHealthScale.playerLowMH = Controller_Player.Instance.playerMentalHealth_Min;
            }
        }

        #endregion
    }
}