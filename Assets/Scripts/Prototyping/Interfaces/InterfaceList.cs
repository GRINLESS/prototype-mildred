﻿/// <summary>
/// Interface used to call menu actions. 
/// </summary>
public interface IPreform {
    void PreformTask();
}

#region Menu Interfaces

/// <summary>
/// Interface used to access a menu items functionality.
/// </summary>
public interface IMenuItemAction {
    void StartAction();
    void IsHighlighted();
    void IsUnhighlighted();
}

#endregion

#region Pause Menu Interfaces

namespace PauseMenuSystem {

    public interface IPauseMenuAction {
        void StartAction();
    }

    /// <summary>
    /// Interface for pause menu items and method calls.
    /// </summary>
    public interface IPauseMenuItems {
        void MenuItemHighlight();
        void MenuItemUnhighlight();
        void Activate();
    }
}

#endregion

#region Gameobject Interfaces
/// <summary>
/// Interface used for LifeTimer to call a destruct.
/// </summary>
public interface ILifeSpan {
    void IsDead();
}

/// <summary>
/// Interface used for interactable object detections.
/// </summary>
namespace ObjectGame.Interactions {
    public interface IInteractable {
        void InteractionActivate(); //Used to call interaction event. 
    }
}
#endregion
