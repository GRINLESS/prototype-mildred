﻿using Mildred_UI.DeathUI;
using Mildred_UI.HealthUI;
using Mildred_UI.TextDisplay;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mildred_UI {
    /// <UIManager>
    /// Used to hold method calls to UI elements without having them exposed. 
    /// </summary>
    public class UIManager : MonoBehaviour {
        #region Singleton
        //Singleton used for referencing UI calls. 
        private static UIManager instance;
        public static UIManager Instance {
            get { return instance; }
            internal set { instance = value; }
        }
        #endregion

        //Component references.
        public HeartManager heartManager;
        public UIDeathScreen uiDeathScreen;

        /// <Awake>
        /// Set the singleton.
        /// </summary>
        private void Awake() {
            if (instance == null) {
                instance = this;
            } else {
                DestroyImmediate(this);
            }
        }

        /// <SetHealthState>
        /// Used to pass health modifications 
        /// without the player controller having to access the UI health element.
        /// </summary>
        /// <param name="currentHealth"> The current health value of player. </param>
        /// <param name="maxHealth"> The max health value for the player. </param>
        public void SetHealthState(float currentHealth, float maxHealth) {
            heartManager.UpdateHealthValue(currentHealth, maxHealth);
        }

        public void SetIemDescriptionPopup(TextAsset asset) {
            if (InteractionTextHandler.Instance != null) {
                InteractionTextHandler.Instance.ImportTextAsset(asset);
            }
        }

        /// <SetTheDeathScreenActive>
        /// Passes the call to the UI Death Screen manager.
        /// </summary>
        public void SetTheDeathScreenActive() {
            uiDeathScreen.SetDeathScreenActive();
        }
    }
}
