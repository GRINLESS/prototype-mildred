﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Mildred_UI.HealthUI {
    public class HeartManager : MonoBehaviour {

        //List of health icons.
        public List<HealthIcon> HEALTHICONS = new List<HealthIcon>();

        private void Awake() {
            foreach (HealthIcon item in HEALTHICONS) {
                item.HEART.enabled = false;
            }
        }

        /// <summary>
        /// Update the health value for UI.
        /// </summary>
        /// <param name="health"> Current health value. </param>
        /// <param name="maxHealth"> Maximum helath value. </param>
        public void UpdateHealthValue(float health, float maxHealth) {
            foreach (HealthIcon HI in HEALTHICONS) {
                if (HI.HEARTID <= health) {
                    HI.HEART.enabled = true;
                } else {
                    HI.HEART.enabled = false;
                }
            }
        }

        /// <summary>
        /// Data class for Heart UI components.  
        /// </summary>
        [System.Serializable]
        public class HealthIcon {
            public string TITLE = "Heart "; //The title of the health icon.
            public int HEARTID; //The id of the health value of the heart.
            public Image HEART; //The image reference. 
        }
    }
}