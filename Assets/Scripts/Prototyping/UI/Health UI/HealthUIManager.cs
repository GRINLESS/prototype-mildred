﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mildred_UI.HealthUI {
    public class HealthUIManager : MonoBehaviour {

        public void SetHealth() { }

        public void SetMaxHealth() { }

        public void Death() { }
    }
}
