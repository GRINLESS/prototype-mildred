﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Mildred_UI.DeathUI {
    public class UIDeathScreen : MonoBehaviour {
        //Reference to the death screen.
        public GameObject DeathScreen;

        /// <SetDeathScreenActive>
        /// Uses the deathScreen object to enable the death screen.
        /// </summary>
        public void SetDeathScreenActive() {
            DeathScreen.SetActive(true);
        }

    }
}
