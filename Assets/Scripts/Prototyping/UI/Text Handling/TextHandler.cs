﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mildred_UI.Internal.TextHandling;

namespace Mildred_UI.TextDisplay {
    public class TextHandler : MarkupIdentifers {

        public TextAsset textFile;

        public List<Character> characters;
        string[] seperatedLines;
        public Text dialogueDisplayTextbox;
        public Text characterDisplayTextbox;

        public void Start() {
            characters = new List<Character>();
            Setup(textFile);
        }

        public void Setup(TextAsset textFile) {
            SetNames(textFile.text);
        }

        #region Set Names

        /// <summary>
        /// Set character names.
        /// </summary>
        /// <param name="fullImport"> The text to sort. </param>
        private void SetNames(string fullImport) {

            //Seperate text using seperator ID
            seperatedLines = SplitAtIdentifier(seperator, fullImport);

            //Find strings containing character name identifiers
            List<string> characterNames = CompareStringToIdentifier(characterName, seperatedLines);

            //Create Character classes for names
            foreach (string name in characterNames) {
                Character Chara = new Character();
                Chara.Name = name;
                characters.Add(Chara);
            }

            SetText(textFile.text);
        }

        #endregion

        #region Set Text

        /// <summary>
        /// Set text lines from document.
        /// </summary>
        /// <param name="fullImport"> Text to sort. </param>
        public void SetText(string fullImport) {

            //Find dialogue lines.
            List<string> dialogueLines = CompareStringToIdentifier(characterDialouge, seperatedLines);

            //Sort them by character
            foreach (Character character in characters) {
                foreach (string string_ in dialogueLines) {
                    if (string_.Contains(character.Name)) {
                        //Remove character name from dialouge line. 
                        string[] cleanoutName = string_.Split(
                            new string[] { character.CharacterIdentifier.IdentifierString }
                            , System.StringSplitOptions.RemoveEmptyEntries);
                        //Add line to character file. 

                        character.linesOfText.Add(cleanoutName[0]);
                    }
                }
            }
        }
        #endregion
    }
}
