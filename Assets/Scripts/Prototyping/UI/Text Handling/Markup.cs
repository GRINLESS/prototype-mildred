﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mildred_UI.Internal.TextHandling {
    public class MarkupIdentifers : MonoBehaviour {

        #region Identifiers
        /// <summary>
        /// Identifies line breaks.
        /// </summary>
        protected Identifer seperator = new Identifer("|");
        /// <summary>
        /// Identifies item lines. 
        /// </summary>
        protected Identifer item = new Identifer("<:Item:>");
        /// <summary>
        /// Identifies Name lines.
        /// </summary>
        protected Identifer itemName = new Identifer("<:Name:>");
        /// <summary>
        /// Identifies description lines. 
        /// </summary>
        protected Identifer itemDescription = new Identifer("<:Description:>");
        /// <summary>
        /// Identifies a character name line.
        /// </summary>
        protected Identifer characterName = new Identifer("<:CName:>");
        /// <summary>
        /// Identifies a character dialouge line. 
        /// </summary>
        protected Identifer characterDialouge = new Identifer("<:Text:>");
        #endregion

        /// <summary>
        /// Split string by a marked identifier.
        /// </summary>
        /// <param name="identifer"> The indentifier.</param>
        /// <param name="stringToSplit"> String to split up. </param>
        /// <returns> List of split strings. </returns>
        protected string[] SplitAtIdentifier(Identifer identifer, string stringToSplit) {
            string[] split =
                stringToSplit.Split(new string[] {
                identifer.IdentifierString },
                    System.StringSplitOptions.RemoveEmptyEntries);
            return split;
        }

        /// <summary>
        /// Find strings marked with identifier and return as list. 
        /// </summary>
        /// <param name="identifer"> The identifier to compare. </param>
        /// <param name="strings"> The list of strings to compare to. </param>
        /// <returns> List of compared strings. </returns>
        protected List<string> CompareStringToIdentifier(Identifer identifer, string[] strings) {
            List<string> stringList = new List<string>();
            foreach (string _string in strings) {
                if (_string.Contains(identifer.IdentifierString)) {
                    string[] stringsSplit = SplitAtIdentifier(identifer, _string);
                    stringList.Add(stringsSplit[0]);
                }
            }
            return stringList;
        }

        /// <summary>
        /// Find a string matching the identifer & remove identifier; 
        /// </summary>
        /// <param name="identifer"> The identifier to find. </param>
        /// <param name="strings"> The strings to compare. </param>
        /// <returns> String matched with value and removed identifier tags. </returns>
        protected string ReturnStringFromIdentifier(Identifer identifer, List<string> strings) {

            string string_ = "";

            foreach (string _string in strings) {
                if (_string.Contains(identifer.IdentifierString)) {
                    string[] stringsSplit = SplitAtIdentifier(identifer, _string);
                    return string_ = stringsSplit[0];
                }
            }
            return string_;
        }
    }

    /// <summary>
    /// Class for assigning text identifiers. 
    /// </summary>
    public class Identifer {
        /// <summary>
        /// The identifier string. 
        /// </summary>
        public string IdentifierString;
        /// <summary>
        /// Creates new Identifier.
        /// </summary>
        /// <param name="_indentifierString"> The identifier string to assign to class. </param>
        public Identifer(string _indentifierString) {
            IdentifierString = _indentifierString;
        }
    }

    /// <summary>
    /// Class used for storing character text data. 
    /// </summary>
    [System.Serializable]
    public class Character {
        /// <summary>
        /// The character name. 
        /// </summary>
        public string Name {
            get { return name; }
            set {
                name = value;
                characterIdentifier = new Identifer("<:" + name + ":>");
            }
        }
        public string name;
        /// <summary>
        /// Character identifier, set when name is set.
        /// </summary>
        public Identifer CharacterIdentifier {
            get { return characterIdentifier; }
            protected set { characterIdentifier = value; }
        }
        private Identifer characterIdentifier;

        /// <summary>
        /// List of dialouge for the character. 
        /// </summary>
        public List<string> linesOfText = new List<string>();
    }
}
