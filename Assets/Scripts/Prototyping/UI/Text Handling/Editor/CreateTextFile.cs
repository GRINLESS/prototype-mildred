﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using System.Text;

public class CreateTextFile : Editor{
    [MenuItem("Assets/Create/Custom/DescriptionTextfile")]
    static void DescriptionTextfile() {
        string path = GetSelectionPath();

        WriteFile_Description(path);

    }

    [MenuItem("Assets/Create/Custom/DialogueTextfile")]
    static void DialogueTextfile() {

        string path = GetSelectionPath();

        WriteFile_Dialogue(path);
    }

    static string GetSelectionPath() {
        string path = "Assets";
        foreach (Object obj in Selection.GetFiltered(typeof(Object), SelectionMode.Assets)) {
            path = AssetDatabase.GetAssetPath(obj);
            if (File.Exists(path)) {
                path = Path.GetDirectoryName(path);
            }
        }
        return path;
    }

    static void WriteFile_Dialogue(string Path) {
        string pathToWrite = Path + "/NewDialogueTextFile.txt";
        if (!Directory.Exists(Path)) {
            return;
        }
        if (!File.Exists(pathToWrite)) {
            using (StreamWriter outFile = new StreamWriter(pathToWrite)) {
                outFile.WriteLine(
                    "|<:CName:>-Insert Character name -<:CName:>|");
                outFile.WriteLine(
                    "|<:Text:><:CharactersName:>-Insert text here -<:Text:>|");

            }
        }
        AssetDatabase.Refresh();
    }

    static void WriteFile_Description(string Path) {

        string pathToWrite = Path + "/NewDescriptionTextFile.txt";
        if (!Directory.Exists(Path)) {
            return;
        }
        if (!File.Exists(pathToWrite)) {
            using (StreamWriter outFile = new StreamWriter(pathToWrite)) {
                outFile.WriteLine("|<:Item:><:Name:>-ItemName<:Name:><:Item:>|");
                outFile.WriteLine("|<:Item:><:Description:>-Item description here-<:Description:><:Item:>|");

            }
        }
        AssetDatabase.Refresh();
    }
}
