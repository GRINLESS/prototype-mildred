﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using Mildred_UI.Internal.TextHandling;

namespace Mildred_UI.TextDisplay {
    public class InteractionTextHandler : MarkupIdentifers {

        public static InteractionTextHandler Instance {
            get { return instance; }
            protected set {
                instance = value;
            }
        }
        private static InteractionTextHandler instance;

        public TextAsset asset;

        #region Component References
        /// <summary>
        /// Used to control display visibility.
        /// </summary>
        [Tooltip("Gameobject containing all related display UI.")]
        public GameObject interactionController;
        /// <summary>
        /// The text component used to display the item name.
        /// </summary>
        [Tooltip("Text field used to display item name. ")]
        public Text text_NameArea;
        /// <summary>
        /// The text component used to display the description. 
        /// </summary>
        [Tooltip("Text field used to display item description. ")]
        public Text text_DescriptionArea;
        #endregion

        #region Internal values
        /// <summary>
        /// The time to display.
        /// </summary>
        const float _displaytime = 4f;
        /// <summary>
        /// Is display active?
        /// </summary>
        private bool _displayActive = false;
        /// <summary>
        /// Has an active display been intterupted?
        /// </summary>
        private bool _currentDisplayIntterupted = false;
        #endregion

        public void Start() {
            if (instance == null) {
                instance = this;
            } else {
                DestroyImmediate(this);
            }

            if (interactionController.activeSelf == true) {
                interactionController.SetActive(false);
            }
        }

        #region Import text
        /// <summary>
        /// Decompile text imported to readable english and store. 
        /// </summary>
        public void ImportTextAsset(TextAsset asset) {

            List<string> strings = new List<string>();
            string fullImport = asset.text;

            //Seperate text using seperator key
            string[] split = SplitAtIdentifier(seperator, fullImport);

            //Seperate using item identifier
            strings = CompareStringToIdentifier(item, split);

            //Find the string containing item name. 
            string ItemName = ReturnStringFromIdentifier(itemName, strings);

            //Find string marked with description identifier.
            string ItemDescription = ReturnStringFromIdentifier(itemDescription, strings);

            SetText(ItemName, ItemDescription);
        }
        #endregion

        #region Set text to display
        /// <summary>
        /// Sets text to the diplay.
        /// </summary>
        public void SetText(string _itemName, string _itemDescription) {
            if (_displayActive) {
                _currentDisplayIntterupted = true;
                interactionController.SetActive(false);
                StopAllCoroutines();
            }
            //Set text to the item name text field.
            text_NameArea.text = _itemName;
            //Set text to the item description text field.
            text_DescriptionArea.text = _itemDescription;
            //Enable the display.
            interactionController.SetActive(true);
            _displayActive = true;
            _currentDisplayIntterupted = false;
            //Start the timer. 
            StartCoroutine(DisplayTimer());
        }

        #endregion

        /// <summary>
        /// Used to time the display. 
        /// </summary>
        IEnumerator DisplayTimer() {
            yield return new WaitForSeconds(_displaytime);
            if (!_currentDisplayIntterupted) {
                ClearDisplay();
            }
        }

        void ClearDisplay() {
            text_NameArea.text = "";
            text_DescriptionArea.text = "";
            interactionController.SetActive(false);
            asset = null;
            _displayActive = false;
        }
    }
}
