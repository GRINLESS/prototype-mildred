﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Resolution {
    /// <summary>
    /// Class used to create resolution settings.  
    /// </summary>
    [System.Serializable]
    public class _Resolution {

        public string _name;
        public int _width;
        public int _height;
        public bool resolutionFlag = false;

        /// <summary>
        /// Constructor used to create resolution. 
        /// </summary>
        /// <param name="name"> Title of the resolution. </param>
        /// <param name="width"> Width of the resolution. </param>
        /// <param name="height"> Height of the resolution. </param>
        public _Resolution(string name, int width, int height) {
            _name = name;
            _width = width;
            _height = height;
        }
    }
}
