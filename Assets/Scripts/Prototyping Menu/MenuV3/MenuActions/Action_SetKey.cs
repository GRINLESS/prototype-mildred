﻿using KeyInputMonitoring;
using Menu.AbstractClasses;
using Menu.Enums;
using UnityEngine;
using UnityEngine.UI;

namespace Menu.Actions {
    public class Action_SetKey : Menu_BaseItems {

        [Tooltip("Text to highlight on display")]
        public Text highlightableText;
        [Tooltip("Text to highlight on display")]
        public Text keyText;

        private KeyCode key;

        public KeyType _keyType = new KeyType();

        /// <summary>
        /// Starts the key rebind when called. 
        /// </summary>
        public override void StartAction() {
            MenuAnimation_DisplayWaitTimer timer = GetComponent<MenuAnimation_DisplayWaitTimer>();
            timer.InteractionAnimationCall();
            switch (_keyType) {
                case KeyType.key_Up:
                    KeyInputs.Instance.data_Keycodes.k_Up.GetNewKey(keyText, timer);
                    break;
                case KeyType.key_Down:
                    KeyInputs.Instance.data_Keycodes.k_Down.GetNewKey(keyText, timer);
                    break;
                case KeyType.key_Right:
                    KeyInputs.Instance.data_Keycodes.k_Right.GetNewKey(keyText, timer);
                    break;
                case KeyType.key_Left:
                    KeyInputs.Instance.data_Keycodes.k_Left.GetNewKey(keyText, timer);
                    break;
                case KeyType.key_Interact:
                    KeyInputs.Instance.data_Keycodes.k_Interact.GetNewKey(keyText, timer);
                    break;
                case KeyType.key_Return:
                    KeyInputs.Instance.data_Keycodes.k_Return.GetNewKey(keyText, timer);
                    break;
                default:
                    break;
            }
        }
    }
}
