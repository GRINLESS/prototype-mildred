﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Menu.Actions {
    /// <summary>
    /// Creates a new game.
    /// </summary>
    public class Action_NewGame : Menu_BaseActivateAction {

        public override void StartAction() {

            if (DataCarrier.instance != null) {
                DataCarrier.instance.SetNewPlayerSaveData();
                DataCarrier.instance.SaveData();
            }

            LevelManagement.LoadLevel(2);
        }
    }
}
