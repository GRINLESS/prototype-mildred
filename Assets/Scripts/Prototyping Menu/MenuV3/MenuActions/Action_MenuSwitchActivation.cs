﻿using Menu.AbstractClasses;
using Menu.LowLevel;
using UnityEngine;
using UnityEngine.UI;

namespace Menu.Actions {
    public class Action_MenuSwitchActivation : Menu_BaseItems {

        public Menu_Base menuToActivate;

        public override void StartAction() {
            if (menuToActivate != null) { //if menu set
                Menu_MainController.Instance.current = menuToActivate; //Set it as current menu
                if (DataCarrier.instance != null) {//If save data is present 
                    DataCarrier.instance.SaveData(); //Save
                } else { //Debug statements.
                    if (DataCarrier.instance == null) {
                        Debug.Log("No Save data : Menu_BaseMenuSwitchActivation :  " + gameObject.name);
                    }
                }
            } else {
                if (menuToActivate == null) {
                    Debug.Log("No menu set : Menu_BaseMenuSwitchActivation :  " + gameObject.name);
                }
            }
        }
    }
}
