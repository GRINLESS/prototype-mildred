﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Menu.Actions {
    /// <summary>
    /// Loads the saved game. 
    /// </summary>
    public class Action_LoadGame : Menu_BaseActivateAction {

        public GameObject noSaveFileDisplay;

        public override void OnEnable() {
            if (!DataCarrier.saveDataPlayer.hasSavedGameBefore) {
                noSaveFileDisplay.SetActive(true);
            } else {
                noSaveFileDisplay.SetActive(false);
            }
            base.OnEnable();
        }

        public override void StartAction() {
            if (!DataCarrier.saveDataPlayer.hasSavedGameBefore) {
                Debug.Log("Player has no save file data.");
                return;
            } else {
                //TODO: Add data load implementation here
                LevelManagement.LoadLevel(2);
            }
        }
    }
}
