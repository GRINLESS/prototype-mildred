﻿using UnityEngine;
using Resolution;

namespace Menu.Actions {
    public class Action_SetResolution : Menu_BaseActivateAction {

        public bool Resolution_1600x900 = false, Resolution_1920x1080 = false, Resolution_1366x768 = false;

        _Resolution thisResolution;

        public override void OnEnable() {
            if (thisResolution == null) {
                if (Resolution_1366x768) {
                    thisResolution = new _Resolution("1366x768", 1366, 768);
                }
                if (Resolution_1600x900) {
                    thisResolution = new _Resolution("1600x900", 1600, 900);
                }
                if (Resolution_1920x1080) {
                    thisResolution = new _Resolution("1920", 1920, 1080);
                }
            }
            base.OnEnable();
        }

        public override void StartAction() {
            Debug.Log("Started Resolution assignment");
            if (DataCarrier.saveDataGameConfig != null) {
                DataCarrier.saveDataGameConfig.resolution = thisResolution;
                CreateScreenOperation createScreenOperation = 
                    new CreateScreenOperation(DataCarrier.saveDataGameConfig.resolution, DataCarrier.saveDataGameConfig.fullscreen);
            }
        }
    }
}

namespace Resolution {

    using UnityEngine;

    public class CreateScreenOperation {
        public CreateScreenOperation(_Resolution resolution, bool fullscreen) {
            GameObject obj = new GameObject();
            UpdateScreen updateScreen = obj.AddComponent<UpdateScreen>();
            updateScreen.UpdateScreenOP(resolution, fullscreen);
        }
    }

    public class UpdateScreen : MonoBehaviour {

        public void UpdateScreenOP(_Resolution _res, bool _fullscreen) {
            Screen.SetResolution(_res._width, _res._height, _fullscreen);
            DestroyImmediate(gameObject);
        }
    }
}
