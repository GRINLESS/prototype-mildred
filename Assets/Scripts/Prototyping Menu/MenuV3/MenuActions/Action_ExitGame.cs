﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Menu.Actions {
    /// <summary>
    /// Exits game. 
    /// </summary>
    public class Action_ExitGame : Menu_BaseActivateAction {

        public override void StartAction() {
            if (DataCarrier.instance != null) {
                DataCarrier.instance.SaveData();
            }

            Application.Quit();
        }
    }
}
