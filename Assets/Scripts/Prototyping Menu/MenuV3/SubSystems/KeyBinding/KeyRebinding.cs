﻿using Input_KeyFunctionality;
using KeyRebind.Data;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Menu.KeyRebind {

    public class Get_KeyOP : MonoBehaviour {

        public KeyCode k;

        public KeyAssignData _data;

        Text _text;

        bool remapActive = false;

        KeyBindableList list;

        /// <summary>
        /// Rebind assigned key. 
        /// </summary>
        /// <param name="data"> Class to be reassigned. </param>
        /// <param name="text"> The output text area. </param>
        public void KeyRebind(KeyAssignData data, Text text, MenuAnimation_DisplayWaitTimer timer) {
            //MAIN_MenuController.Instance.dontUpdateInput = true;
            list = new KeyBindableList();
            _data = data;
            _text = text;
            StartCoroutine(WaitBeforeKeyAssignStarts(timer));
        }

        //Loop through key assigning loop if remapping.
        private void Update() {
            if (!remapActive) {
                return;
            }
            if (KeyCode.None == k) {
                foreach (KeyCode key in KeyBindableList.bindableKeysAlphabet) {
                    if (Input.GetKey(key)) {
                        k = key;
                        break;
                    }
                }
                foreach (KeyCode key in KeyBindableList.bindableKeysSpecial) {
                    if (Input.GetKey(key)) {
                        k = key;
                        break;
                    }
                }
            }
            if (KeyCode.None != k) {
                //Deactivate the key mapping
                remapActive = false;

                bool isAssignedCurrently = KeyAssignCheck();

                if (!isAssignedCurrently) {
                    //Set the key.
                    StartCoroutine(WaitTillMenuInputReturned());
                } else {
                    //Do not display.
                    StartCoroutine(WaitTillMenuInputNotAssigned());
                }

            }
        }

        /// <summary>
        /// Check key assignment against already assigned keys.
        /// </summary>
        /// <returns> false : if not assigned, true: if assigned.</returns>
        private bool KeyAssignCheck() {
            if (k == KeyInputMonitoring.KeyInputs.Instance.data_Keycodes.k_Up.Key) {
                return true;
            } else if (k == KeyInputMonitoring.KeyInputs.Instance.data_Keycodes.k_Down.Key) {
                return true;
            } else if (k == KeyInputMonitoring.KeyInputs.Instance.data_Keycodes.k_Left.Key) {
                return true;
            } else if (k == KeyInputMonitoring.KeyInputs.Instance.data_Keycodes.k_Right.Key) {
                return true;
            } else if (k == KeyInputMonitoring.KeyInputs.Instance.data_Keycodes.k_Interact.Key) {
                return true;
            } else if (k == KeyInputMonitoring.KeyInputs.Instance.data_Keycodes.k_Return.Key) {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Activate countdown before the key assigning.
        /// </summary>
        /// <returns> bool flagging keymapping active. </returns>
        IEnumerator WaitBeforeKeyAssignStarts(MenuAnimation_DisplayWaitTimer _timer) {
            _text.text = "Please Wait 5";
            yield return new WaitForSeconds(100f * Time.deltaTime);
            _text.text = "Please Wait 4";
            yield return new WaitForSeconds(100f * Time.deltaTime);
            _text.text = "Please Wait 3";
            yield return new WaitForSeconds(100f * Time.deltaTime);
            _text.text = "Please Wait 2";
            yield return new WaitForSeconds(100f * Time.deltaTime);
            _text.text = "Please Wait 1";
            yield return new WaitForSeconds(100f * Time.deltaTime);
            _text.text = "Press Key.";
            remapActive = true;
            k = KeyCode.None;
            _timer.InteractionExitAnimationCall();
        }

        /// <summary>
        /// Start delay before menu control returned. 
        /// </summary>
        /// <returns></returns>
        IEnumerator WaitTillMenuInputReturned() {
            _data.Key = k;
            _text.text = k.ToString();
            yield return new WaitForSeconds(150f * Time.deltaTime);
            list = null;
            GameObject.Destroy(gameObject);
        }

        /// <summary>
        /// Display invalid key assignment, return menu control after delay. 
        /// </summary>
        /// <returns></returns>
        IEnumerator WaitTillMenuInputNotAssigned() {
            _text.text = "Invalid key.";
            yield return new WaitForSeconds(150f * Time.deltaTime);
            _text.text = _data.Key.ToString();
            list = null;
            GameObject.Destroy(gameObject);
        }
    }
}
