﻿using Menu.AbstractClasses;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Menu.PauseMenuActions {
    public class Menu_ReturnToGame : Menu_BaseItems {
        //public override void IsHighlighted() {
        //    base.text.color = base.colour2;
        //}

        //public override void IsUnhighlighted() {
        //    base.text.color = base.colour1;
        //}

        public override void StartAction() {
            Menu_PauseMenu.Instance.EnableMenu = false;
        }
    }
}
