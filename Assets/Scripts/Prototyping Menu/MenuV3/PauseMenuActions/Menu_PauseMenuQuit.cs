﻿using Menu.AbstractClasses;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MenuAnimation.Core;

namespace Menu.PauseMenuActions {
    public class Menu_PauseMenuQuit : Menu_BaseItems {

        /// <summary>
        /// Saves player data & load main menu.
        /// </summary>
        public override void StartAction() {
            if (animations != null) {
                WaitForAnimation();
            }

            //Load menu
            LevelManagement.LoadLevel(LevelManagement.main);
        }

        IEnumerator WaitForAnimation() {
            List<bool> completedList = new List<bool>();
            foreach (Base_MenuAnimation animation in animations) {
                completedList.Add(animation.InteractionAnimationCall());
            }
            //Start the activation animation
            

            while (completedList.Count > 0) {
                yield return new WaitForSeconds(1f);
                foreach (bool flag in completedList) {
                    if (flag == true) {
                        completedList.Remove(flag);
                    }
                }
            }
        }
    }
}