﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MenuAnimation.Core;

public class MenuAnimation_DisplayWaitTimer : Base_MenuAnimation {

    private Image displayArea;
    private Animator Animator; 

    public void OnEnable() {
        displayArea = GetComponentInChildren<Image>();
        Animator = GetComponentInChildren<Animator>();
    }

    public override void ActivationAnimationCall() {
        
    }

    public override void DeactivationAnimationCall() {
    }

    private bool active = false;

    public override bool InteractionAnimationCall() {
        active = true;
        StartCoroutine(InteractionActive());
        return true;
    }

    public override bool InteractionExitAnimationCall() {
        active = false;
        return true;
    }

    private IEnumerator InteractionActive() {
        Animator.SetBool("Active", active);
        while (active == true) {
            yield return new WaitForSeconds(1f * Time.deltaTime);
        }
        Animator.SetBool("Active", active);
    }
}
