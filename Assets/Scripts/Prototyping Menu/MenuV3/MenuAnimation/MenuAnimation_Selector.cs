﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MenuAnimation.Core;

namespace MenuAnimation {
    public class MenuAnimation_Selector : Base_MenuAnimation {

        public Image Indicator1, Indicator2;

        public override void ActivationAnimationCall() {
            if (Indicator1 != null) {
                Indicator1.gameObject.SetActive(true);
            }
            if (Indicator2 != null) {
                Indicator2.gameObject.SetActive(true);
            }
        }

        public override void DeactivationAnimationCall() {
            if (Indicator1 != null) {
                Indicator1.gameObject.SetActive(false);
            }
            if (Indicator2 != null) {
                Indicator2.gameObject.SetActive(false);
            }
        }

        public override bool InteractionAnimationCall() {
            throw new System.NotImplementedException();
        }

        public override bool InteractionExitAnimationCall() {
            throw new System.NotImplementedException();
        }
    }
}
