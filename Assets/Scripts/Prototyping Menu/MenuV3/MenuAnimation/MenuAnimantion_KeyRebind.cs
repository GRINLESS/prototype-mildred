﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Menu.Actions;
using MenuAnimation.Core;
using Menu.Enums;

namespace MenuAnimation {
    public class MenuAnimantion_KeyRebind : Base_MenuAnimation {

        KeyType keyType = new KeyType();

        public Text KeyName, KeyAssigned;
        public Image active, complete;

        private void OnEnable() {
            //Get the info for text boxes.
            Action_SetKey setKey = gameObject.GetComponent<Menu.Actions.Action_SetKey>();
            keyType = setKey._keyType;
            //Set the key name and current assigned key.
            switch (keyType) {
                case KeyType.key_Up:
                    KeyName.text = "Up";
                    KeyAssigned.text = DataCarrier.saveDataGameConfig.data_Keycodes.k_Up.Key.ToString();
                    break;
                case KeyType.key_Down:
                    KeyName.text = "Down";
                    KeyAssigned.text = DataCarrier.saveDataGameConfig.data_Keycodes.k_Down.Key.ToString();
                    break;
                case KeyType.key_Right:
                    KeyName.text = "Right";
                    KeyAssigned.text = DataCarrier.saveDataGameConfig.data_Keycodes.k_Right.Key.ToString();
                    break;
                case KeyType.key_Left:
                    KeyName.text = "Left";
                    KeyAssigned.text = DataCarrier.saveDataGameConfig.data_Keycodes.k_Left.Key.ToString();
                    break;
                case KeyType.key_Interact:
                    KeyName.text = "Interact";
                    KeyAssigned.text = DataCarrier.saveDataGameConfig.data_Keycodes.k_Interact.Key.ToString();
                    break;
                case KeyType.key_Return:
                    KeyName.text = "Return";
                    KeyAssigned.text = DataCarrier.saveDataGameConfig.data_Keycodes.k_Return.Key.ToString();
                    break;
                default:
                    break;
            }
        }

        public override void ActivationAnimationCall() {

        }

        public override void DeactivationAnimationCall() {

        }

        public override bool InteractionAnimationCall() {
            return true;
        }

        public override bool InteractionExitAnimationCall() {
            return true;
        }
    }
}
