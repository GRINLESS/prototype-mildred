﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MenuAnimation.Core;

namespace MenuAnimation {
    public class MenuAnimation_TextHighlighter : Base_MenuAnimation {
        public Text text;
        public Color colour_Highlight, colour_NotHighlighted, colour_Selected;

        public override void ActivationAnimationCall() {
            if (text != null) {
                text.color = colour_Highlight;
            }
        }

        public override void DeactivationAnimationCall() {
            if (text != null) {
                text.color = colour_NotHighlighted;
            }
        }

        public override bool InteractionAnimationCall() {
            throw new System.NotImplementedException();
        }

        public override bool InteractionExitAnimationCall() {
            throw new System.NotImplementedException();
        }
    }
}
