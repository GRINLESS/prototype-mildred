﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MenuAnimation.Core {
    public abstract class Base_MenuAnimation : MonoBehaviour {

        /// <summary>
        /// Call activation animation of a menu option.
        /// </summary>
        public abstract void ActivationAnimationCall();
        /// <summary>
        /// Call deactivation animation of a menu option. 
        /// </summary>
        public abstract void DeactivationAnimationCall();
        /// <summary>
        /// Call interaction animation of a menu option. 
        /// </summary>
        public abstract bool InteractionAnimationCall();

        public abstract bool InteractionExitAnimationCall();
    }
}
