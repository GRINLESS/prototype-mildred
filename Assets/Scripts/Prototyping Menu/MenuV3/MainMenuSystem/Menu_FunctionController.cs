﻿using KeyInputMonitoring;
using Menu.AbstractClasses;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Menu.LowLevel {
    public class Menu_FunctionController : MonoBehaviour {

        #region Menu Position Fields
        public int _currentMenuPos;
        public int minMenuPos;
        public int maxMenuPos;
        #endregion

        public bool dontUpdateInput = false;
        public MenuInputs mInputs = new MenuInputs();
        public static bool isMenuActive = true;
        protected float menuDelay = 75f;
        public static float currentDelay = 75f; 
        private void Start() {
            if (KeyInputs.Instance == null) {
                gameObject.AddComponent<KeyInputs>();
            }
            currentDelay = menuDelay;
        }

        #region Menu Position Updating

        /// <summary>
        /// Sets the menu length.
        /// </summary>
        /// <param name="length"> Value used to set menu length. </param>
        public virtual void SetMenuLength(int length) {
            _currentMenuPos = 0;
            minMenuPos = 0;
            maxMenuPos = length - 1;
        }

        /// <summary>
        /// Update current menu position.
        /// </summary>
        /// <param name="up"> Input bool for down input. </param>
        /// <param name="down"> Input bool for up input. </param>
        public int UpdateMenuPosition(bool up, bool down) {
            if (down) {
                _currentMenuPos++;
            }
            if (up) {
                _currentMenuPos--;
            }
            if (_currentMenuPos > maxMenuPos) {
                _currentMenuPos = minMenuPos;
            }
            if (_currentMenuPos < minMenuPos) {
                _currentMenuPos = maxMenuPos;
            }
            return _currentMenuPos;
        }
        #endregion

        #region Menu Graphical Updating

        /// <summary>
        /// Update the menus graphical interface. 
        /// </summary>
        /// <param name="list"> List of items on the menu</param>
        public virtual void UpdateMenuGraphics(List<Menu_BaseItems> list) {
            foreach (Menu_BaseItems MBI in list) {
                MBI.IsUnhighlighted();
                if (MBI.optionID == _currentMenuPos) {
                    MBI.IsHighlighted();
                }
            }
        }

        #endregion

        public virtual void Update() {
            if (!dontUpdateInput) {
                //Update the inputs.
                KeyInputs.Instance.UpdateInputsMenu();
                UpdateInput();
            }
            //Update menu position.
            UpdateMenuPosition(mInputs.down, mInputs.up);
            mInputs.up = false;
            mInputs.down = false;
            if (Menu_MainController.Instance.current != null) {
                UpdateMenuGraphics(Menu_MainController.Instance.current.menuItems);
            }

        }

        #region Input Updating

        /// <summary>
        /// Used to pull updated keypress data into menu.
        /// </summary>
        private void UpdateInput() {
            mInputs.ClearInputs();
            if (KeyInputs.Instance.data_Keycodes.k_Down.state) {
                mInputs.up = true;
                KeyInputs.Instance.data_Keycodes.k_Down.state = false;
            }
            if (KeyInputs.Instance.data_Keycodes.k_Up.state) {
                mInputs.down = true;
                KeyInputs.Instance.data_Keycodes.k_Up.state = false;
            }
            if (KeyInputs.Instance.data_Keycodes.k_Left.state) {
                mInputs.left = true;
                KeyInputs.Instance.data_Keycodes.k_Left.state = false;
            }
            if (KeyInputs.Instance.data_Keycodes.k_Right.state) {
                mInputs.right = true;
                KeyInputs.Instance.data_Keycodes.k_Right.state = false;
            }
            if (KeyInputs.Instance.data_Keycodes.k_Interact.state) {
                mInputs.interact = true;
                KeyInputs.Instance.data_Keycodes.k_Interact.state = false;
            }
            if (KeyInputs.Instance.data_Keycodes.k_Return.state) {
                mInputs.ret = true;
                KeyInputs.Instance.data_Keycodes.k_Return.state = false;
            }
        }


        #endregion

        /// <summary>
        /// Used to time an input delay after menu swap occurs.
        /// </summary>
        protected IEnumerator MenuSwapTimer() {
            dontUpdateInput = true;
            if (currentDelay == menuDelay) {
                yield return new WaitForSeconds(menuDelay * Time.deltaTime);
            } else {
                yield return new WaitForSeconds(currentDelay * Time.deltaTime);
            }
            
            dontUpdateInput = false;
        }


    } 

    [System.Serializable]
    public class MenuInputs {
        public bool
            up = false, down = false, left = false,
            right = false, interact = false, ret = false;

        /// <summary>
        /// Used to clear the current menu inputs.
        /// </summary>
        public void ClearInputs() {
            up = down = left = right = interact = ret = false;
        }
    }
}