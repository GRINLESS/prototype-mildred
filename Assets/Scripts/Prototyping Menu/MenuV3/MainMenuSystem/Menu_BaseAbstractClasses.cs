﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Menu.LowLevel;
using MenuAnimation.Core;

namespace Menu.AbstractClasses {

    public abstract class Menu_Base : MonoBehaviour {

        #region Internal functionality fields

        /// <summary>
        /// Internal functionality: Used to flag wether the menu has focus. 
        /// </summary>
        [HideInInspector]
        public bool hasFocus = false;

        /// <summary>
        /// Internal functionality: Used to flag wether the menu should update. 
        /// </summary>
        [HideInInspector]
        public bool dontUpdateInput;

        #endregion

        /// <summary>
        /// The current menu inputs. 
        /// </summary>
        MenuInputs menuInputs;

        /// <summary>
        /// List of menu Items.
        /// </summary>
        [HideInInspector]
        public List<Menu_BaseItems> menuItems;
        [HideInInspector]
        public GameObject childObj;

        #region Internal Base menu functionality

        //Setup used for main menu.
        private void Start() {
            //Set items on menu.
            SetMenuItems();
            //Set the state of menu.
            SetTheMenuState();

        }

        public virtual void OnEnable() {

        }

        #region Pause Menu Functions
        /// <summary>
        ///Set up pause menu.
        /// </summary>
        public void PauseMenuSetup() {
            
            //Set items on menu.
            SetMenuItems();
            //Set the state of menu.
            SetTheMenuState();
            
        }
        #endregion

        private void Update() {
        }

        /// <summary>
        /// Internal functionality: Gather menu items in a list. 
        /// </summary>
        private void SetMenuItems() {
            //Do a get call for first object, which in menu structure should be the first object. 
            childObj = gameObject.transform.GetChild(0).gameObject;
            childObj.SetActive(true);
            //Set the menu items.
            menuItems = new List<Menu_BaseItems>();
            Menu_BaseItems[] arrary = GetComponentsInChildren<Menu_BaseItems>() as Menu_BaseItems[];

            for (int i = 0; i < arrary.Length + 1; i++) {
                foreach (Menu_BaseItems item in arrary) {
                    if (item.optionID == i) {
                        menuItems.Add(item);
                    }
                }
            }
            if (!hasFocus) {
                childObj.SetActive(false);
            }
        }

        /// <summary>
        /// Internal functionality: Used to set the state of the menu.
        /// </summary>
        private void SetTheMenuState() {
            if (hasFocus == true) {
                Menu_MainController.Instance.current = this;
                Menu_MainController.Instance.SetMenuLength(menuItems.Count);
            }
        }
        #endregion

        #region Abstractive menu functionality
        public abstract void OnDisable();
        #endregion
    }

    public abstract class Menu_BaseItems : MonoBehaviour {

        [Tooltip("Used to set the order in menu (0>)")]
        public int optionID;
        [HideInInspector]
        public Text text;

        #region Text colours
        [HideInInspector]
        public Color32 colour1 = new Color32(171, 171, 171, 255);
        [HideInInspector]
        public Color32 colour2 = new Color32(255, 255, 255, 255);
        #endregion

        #region Animation References
        public List<Base_MenuAnimation> animations;
        #endregion

        public virtual void OnEnable() {
            text = gameObject.GetComponentInChildren<Text>();
            //Set the animation components
            GatherAnimationComponents();
        }

        /// <summary>
        /// Used for graphical changes when selected.
        /// </summary>
        public void IsHighlighted() {
            foreach (Base_MenuAnimation animation in animations) {
                animation.ActivationAnimationCall();
            }
        }

        /// <summary>
        /// Used for graphical changes when unselected.
        /// </summary>
        public void IsUnhighlighted() {
            foreach (Base_MenuAnimation animation in animations) {
                animation.DeactivationAnimationCall();
            }
        }

        /// <summary>
        /// Start the options functionality.
        /// </summary>
        public abstract void StartAction();

        public virtual void ActionCompleted() {
            foreach (Base_MenuAnimation item in animations) {
                item.InteractionExitAnimationCall();
            }
        }

        /// <summary>
        /// Gather the animation components.
        /// </summary>
        private void GatherAnimationComponents() {
            Base_MenuAnimation[] list = gameObject.GetComponentsInChildren<Base_MenuAnimation>() as Base_MenuAnimation[];
            if (list.Length != 0 && GetComponent<Base_MenuAnimation>() != null) {
                animations = new List<Base_MenuAnimation>();
                foreach (Base_MenuAnimation menuAnim in list) {
                    animations.Add(menuAnim);
                }
            }
        }
    }
}