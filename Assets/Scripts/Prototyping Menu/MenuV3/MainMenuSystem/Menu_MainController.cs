﻿using Menu.AbstractClasses;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Menu.LowLevel {
    public class Menu_MainController : Menu_FunctionController {

        #region Singleton
        private static Menu_MainController instance;
        public static Menu_MainController Instance {
            get { return instance; }
            internal set {
                instance = value;
            }
        }
        #endregion

        //[HideInInspector]
        public List<Menu_Base> menus = new List<Menu_Base>();

        //[HideInInspector]
        public Menu_Base current, last;

        void Awake() {
            if (Instance == null) {
                Instance = this;
            } else {
                DestroyImmediate(this);
            }
        }

        private void Start() {
            SetUpMenuCollection();
        }

        /// <summary>
        /// Used to set up menu collection.
        /// </summary>
        void SetUpMenuCollection() {
            Menu_Base[] menuCollection = GetComponentsInChildren<Menu_Base>() as Menu_Base[];
            foreach (Menu_Base B_M in menuCollection) {
                menus.Add(B_M);
            }
        }

        // Update is called once per frame
        public override void Update() {
            CheckIfActiveMenuChanged();
            if (mInputs.interact && isMenuActive) {
                current.dontUpdateInput = true;
                current.menuItems[_currentMenuPos].StartAction();
            }
            base.Update();
        }

        /// <summary>
        /// Has current menu changed. 
        /// </summary>
        void CheckIfActiveMenuChanged() {
            if (current == null && last == null) { //If current is null, return.
                return;
            } else if (current != last) { //If the last assignment is not the same as the current
                StartCoroutine(MenuSwapTimer());
                base._currentMenuPos = 0;
                if (current == null) { //If the current does not hold a referance.
                    last.hasFocus = false;
                    last.childObj.SetActive(false);
                    last = null;
                } else if (current != null) { //If current holds a reference.
                    if (last != null) { //If last is not null.
                        last.childObj.SetActive(false);
                        last.hasFocus = false; //Defocus the last menu.
                    }
                    current.childObj.SetActive(true);
                    current.hasFocus = true; //Focus the new menu.
                    SetMenuLength(current.menuItems.Count); //Fix the menu length. 
                    last = current; //Set the last to current for ref purposes. 
                }
            }
        }

        public static void ResetDelay() {
            instance.ResetDelayFloat();
        }
        public void ResetDelayFloat() {
            currentDelay = instance.menuDelay;
        }
    }

}