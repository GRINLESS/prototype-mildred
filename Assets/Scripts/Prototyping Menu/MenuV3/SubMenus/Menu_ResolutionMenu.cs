﻿using DataSaving;
using Menu.AbstractClasses;
using Resolution;
using UnityEngine;

namespace ResolutionManagement {
    public class Menu_ResolutionMenu : Menu_Base {

        public SaveDataGameConfig SDGC;
        private _SetResolutions referance = new _SetResolutions();

        public override void OnEnable() {
            if (SDGC == null) {
                if (DataCarrier.instance != null) {
                    SDGC = DataCarrier.saveDataGameConfig;
                }
            }
        }

        public override void OnDisable() {
        }

        private void Awake() {
            base.hasFocus = false;
        }

        private void SetFullscreen() {
            SDGC.fullscreen = !SDGC.fullscreen;
            Screen.SetResolution(DataCarrier.saveDataGameConfig.resolution._width,
                                 DataCarrier.saveDataGameConfig.resolution._height,
                                 DataCarrier.saveDataGameConfig.fullscreen);
        }

        public void SetResolution(_Resolution resolution) {
            DataCarrier.saveDataGameConfig.resolution = resolution;

            Screen.SetResolution(DataCarrier.saveDataGameConfig.resolution._width,
                                 DataCarrier.saveDataGameConfig.resolution._height,
                                 DataCarrier.saveDataGameConfig.fullscreen);
        }

    }
}
