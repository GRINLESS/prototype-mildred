﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Menu.AbstractClasses;
using Menu.LowLevel;

public class Menu_GameTitleSplash : Menu_Base {

    public Menu_Base menuToActivate;

    void Awake () {
        base.hasFocus = true;
	}
	
	void Update () {
        if (Input.anyKey && base.hasFocus && 
            !Input.GetMouseButton(0) && 
            !Input.GetMouseButton(1) && 
            !Input.GetMouseButton(2)) {
            Menu_MainController.Instance.current = menuToActivate;
        }
	}

    public override void OnDisable() {
        
    }
}
