﻿using Menu.AbstractClasses;
using Menu.LowLevel;
using System.Collections;
using UnityEngine;

public class Menu_PauseMenu : Menu_Base {
    public static Menu_PauseMenu Instance;

    /// <summary>
    /// Used by player menu to call the change of pause menu state. 
    /// </summary>
    public bool EnableMenu {
        get {
            return menuEnabled;
        }
        set {
            if (canSwapState) {
                //Swap the set state.
                menuEnabled = SwapState();   
            }
        }
    }

    /// <summary>
    /// Flag to store the menu state.
    /// </summary>
    bool menuEnabled = false;
    /// <summary>
    /// Wether the menu state can swap. 
    /// </summary>
    bool canSwapState = true;

    //We override here so we can use the pause menu setup.
    public override void OnEnable() {
        
        base.PauseMenuSetup();
    }

    void Awake() {
        Instance = this;
    }

    private void Start() {
        Menu_FunctionController.isMenuActive = false;
    }

    /// <summary>
    /// Function to swap menu active state.
    /// </summary>
    /// <returns> Returns current menu state. </returns>
    public bool SwapState() {
        menuEnabled = !menuEnabled;
        
        canSwapState = false;
        if (menuEnabled) {
            Menu_MainController.Instance.current = this;
            Controller_Player.Instance.CanUpdate = false;
            Menu_FunctionController.isMenuActive = true;
            Menu_MainController.Instance.SetMenuLength(base.menuItems.Count);
            Menu_FunctionController.currentDelay = 0.5f;
            Time.timeScale = 0.1f;
        } else {
            if (!menuEnabled) {
                Menu_MainController.Instance.current = null;
                Controller_Player.Instance.CanUpdate = true;
                Menu_FunctionController.isMenuActive = false;
                Menu_FunctionController.currentDelay = 75f;
                Time.timeScale = 1;
            }
        }
        StartCoroutine(SwapDelayTimer());
        return menuEnabled;
    }

    /// <summary>
    /// Times the delay between swaps. 
    /// </summary>
    /// <returns></returns>
    IEnumerator SwapDelayTimer() {
        yield return new WaitForSeconds(0.1f * Time.deltaTime);
        canSwapState = true;
    }

    public override void OnDisable() {
        Menu_MainController.ResetDelay();
    }
}
