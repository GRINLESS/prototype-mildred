﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Menu.Enums {
    /// <summary>
    /// Used to set the keys for rebinding in editor.
    /// </summary>
    [System.Serializable]
    public enum KeyType {
        key_Up = 0,
        key_Down = 1,
        key_Right = 2,
        key_Left = 3,
        key_Interact = 4,
        key_Return = 5
    }
}
